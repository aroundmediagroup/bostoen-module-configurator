﻿mergeInto(LibraryManager.library, {

  sendMessageToJavascript: function (messageJson) {
      var message = Pointer_stringify(messageJson);
      if (typeof ReceiveMessageFromUnity === 'function') ReceiveMessageFromUnity(message);
    },

  _LogToJavascript: function (message) {
      var logMessage = Pointer_stringify(message);
      if (typeof LogToJavascript === 'function') LogToJavascript(logMessage);
    }
});