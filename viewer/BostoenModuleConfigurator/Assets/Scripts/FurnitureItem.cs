using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnitureItem : MonoBehaviour
{
    [SerializeField] private Transform _target = null;

    // Update is called once per frame
    void Update()
    {
        if (_target)
        {
            transform.position = _target.position;
        }
    }
}
