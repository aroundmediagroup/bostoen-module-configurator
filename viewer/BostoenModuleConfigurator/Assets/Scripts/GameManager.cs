using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public delegate void ConfirmButtonClickedHandler(ScreenType type);
public delegate void ButtonClickedHandler();
public delegate void SliderValueChangedHandler(float newValue);
public delegate void ToggleValueChangedHandler(bool newValue);
public delegate void SelectContentHandler(int id);

public enum ScreenType
{
    SetSize,
    SetContents,
    SetLabels,
    Overview
}

public class GameManager : MonoBehaviour
{
    [SerializeField] private CameraControll _camera;
    [SerializeField] private Camera _screenshotCamera;
    [SerializeField] private Light _light;
    [SerializeField] private List<RenderTexture> _rendertextures = new List<RenderTexture>();
    [SerializeField] private House _house;

    [SerializeField] private Room[] _modulePrefabs;
    [SerializeField] private Window[] _windowPrefabs;
    [SerializeField] private Door[] _doorPrefabs;
    [SerializeField] private Door[] _insdeDoorPrefabs;
    //UI
    [SerializeField] private SetSizeScreen _setSizeScreen;
    [SerializeField] private SetContentScreen _setContentsScreen;
    [SerializeField] private SetLabelsScreen _setLabelsScreen;
    [SerializeField] private OverviewScreen _overviewScreen;
    [SerializeField] private SelectFloorScreen _selectFloorScreen;
    [SerializeField] private PersistenUI _persistenUI;
    [SerializeField] private ConfirmScreenUI _confirmUI;
    [SerializeField] private InteractionPanel _interactionPanel;
    [SerializeField] private TutorialScreen _tutorialScreen;
    [SerializeField] private TutorialScreen _contactScreen;
    [SerializeField] private CodeScreen _codeScreen;

    private Camera _mainCam = null;

    private Room _currentPlacingModule = null;
    private Window _currentPlacingWindow = null;
    private Door _currentPlacingDoor = null;

    private Room _currentSelectedModule = null;
    private Window _currentSelectedWindow = null;
    private Door _currentSelectedDoor = null;

    private WallPiece _lastInsertWallPiece = null;

    private List<Room> _placedModules = new List<Room>();
    private List<Window> _placedWindows = new List<Window>();
    private List<Door> _placedDoors = new List<Door>();

    private bool _showMeasurements = true;
    private bool _isShowingTutorial = false;

    private ScreenType _currentScreen = ScreenType.SetSize;
    private int _firstFloorType = 0;


    private string _sessionId = "";
    private bool IsWorkingInSession() { return _sessionId.Length > 0; } // if _sessionId is filled in, we are in a session


    void Awake()
    {
        _mainCam = Camera.main;

        MessageHandler.OnViewerInitialized += onInitialize;

        // These calls should be removed, call API directly to load and save a session
        WebPageInteractor.OnMessageReceived += ReceiveMessage;

        //UI
        _setSizeScreen.OnConfirmClicked += ScreenConfirmClicked;
        _setSizeScreen.OnCodeButtonClicked += EnterCodeClicked;
        _setSizeScreen.OnLengthSliderValueChanged += OnLengthChanged;
        _setSizeScreen.OnWidthSliderValueChanged += OnWidthChanged;
        _setSizeScreen.OnExtendFirstFloorValueChanged += OnExtendFirstFloorChanged;

        _setContentsScreen.OnConfirmClicked += ScreenConfirmClicked;
        _setContentsScreen.OnBackClicked += ScreenBackClicked;
        _setContentsScreen.OnModuleSelected += SelectModule;
        _setContentsScreen.OnWindowSelected += SelectWindow;
        _setContentsScreen.OnOutsideDoorSelected += SelectOutsideDoor;
        _setContentsScreen.OnInsideDoorSelected += SelectInsideDoor;
        _setContentsScreen.SetModules(_modulePrefabs);
        _setContentsScreen.SetWindows(_windowPrefabs);
        _setContentsScreen.SetOutsideDoors(_doorPrefabs);
        _setContentsScreen.SetInsideDoors(_insdeDoorPrefabs);

        _setLabelsScreen.OnConfirmClicked += ScreenConfirmClicked;
        _setLabelsScreen.OnBackClicked += ScreenBackClicked;

        _overviewScreen.OnGetCodeClicked += GetCode;
        _overviewScreen.OnBackClicked += ScreenBackClicked;
        _overviewScreen.OnDownloadClicked += DownloadClicked;

        _selectFloorScreen.OnFirstFloorToggleChanged += OnSelectedFloorChanged;

        _persistenUI.OnResetClicked += OnReset;
        _persistenUI.OnRecenterClicked += OnRecenter;
        _persistenUI.OnMesurementsClicked += OnShowMeasurements;
        _persistenUI.OnTutorialClicked += OnShowTutorial;
        _persistenUI.OnContactClicked += OnShowContactScreen;

        _confirmUI.OnConfirmClicked += OnConfirm;
        _confirmUI.OnCancelClicked += OnCancel;
        _confirmUI.SetActive(false);

        _interactionPanel.OnDeleteButtonClicked += OnDeleteObject;
        _interactionPanel.OnMoveButtonDown += OnMoveObject;
        _interactionPanel.OnRotateButtonClicked += OnRotateObject;

        _tutorialScreen.OnCloseClicked += CloseTutorial;
        _contactScreen.OnCloseClicked += CloseContactScreen;

        _codeScreen.OnCloseClicked += CloseCodeScreen;
        _codeScreen.OnConfirmClicked += ConfirmCode;

        SetScreen(ScreenType.SetSize);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && _currentScreen == ScreenType.SetContents)
        {
            bool isDeleteButton = false;
            PointerEventData eventData = new PointerEventData(EventSystem.current);
            eventData.position = Input.mousePosition;
            List<RaycastResult> raysastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, raysastResults);
            for (int i = 0; i < raysastResults.Count; i++)
            {
                if (raysastResults[i].gameObject.CompareTag("InteractionButton"))
                {
                    isDeleteButton = true;
                    break;
                }
            }
            if (isDeleteButton == false)
            {
                _currentSelectedModule = null;
                _currentSelectedWindow = null;
                _currentSelectedDoor = null;
                bool setInteractionPanelAtcive = false;
                Ray ray = _mainCam.ScreenPointToRay(Input.mousePosition);
                LayerMask mask = LayerMask.GetMask("OuterWall", "InnerWall", "Stairs", "Garage");
                mask = ~mask;
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 200, mask))
                {
                    Room room = hit.transform.gameObject.GetComponent<Room>();
                    if (room)
                    {
                        _currentSelectedModule = room;
                        _interactionPanel.SetUITarget(_currentSelectedModule.transform);
                        setInteractionPanelAtcive = true;
                    }
                    else
                    {
                        Window window = hit.transform.gameObject.GetComponent<Window>();
                        if (window)
                        {
                            _currentSelectedWindow = window;
                            _interactionPanel.SetUITarget(_currentSelectedWindow.transform);
                            setInteractionPanelAtcive = true;
                        }
                        else
                        {
                            Door door = hit.transform.gameObject.GetComponent<Door>();
                            if (door)
                            {
                                _currentSelectedDoor = door;
                                _interactionPanel.SetUITarget(_currentSelectedDoor.transform);
                                setInteractionPanelAtcive = true;
                            }
                        }
                    }
                }
                _interactionPanel.SetActive(setInteractionPanelAtcive);
                if (_currentSelectedModule != null)
                {
                    _interactionPanel.SetRotateActive(_currentSelectedModule.ModuleType != ModuleType.Garage);
                }
                else
                {
                    _interactionPanel.SetRotateActive(false);
                }
            }
        }
        else if(Input.GetMouseButton(0))
        {
            if(_currentPlacingModule)
            {
                Ray ray = _mainCam.ScreenPointToRay(Input.mousePosition);
                LayerMask mask;
                if (_currentPlacingModule.ModuleType == ModuleType.Garage)
                {
                    mask = LayerMask.GetMask("Garage");
                }
                else
                {
                    mask = LayerMask.GetMask("GroundFloor", "TopFloor");
                }
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 150, mask))
                {
                    bool isValidPos = true;
                    //Collider[] florrPiecesOverlap = Physics.OverlapBox(hit.collider.transform.position, new Vector3(_currentPlacingModule.Width, 0.5f, _currentPlacingModule.Length), Quaternion.identity, mask);
                    //if (florrPiecesOverlap.Length > 0)
                    //{
                    //    for(int i = 0; i < florrPiecesOverlap.Length; i++)
                    //    {
                    //        if(florrPiecesOverlap[i].GetComponent<FloorPiece>().IsBlocked)
                    //        {
                    //            isValidPos = false;
                    //            break;
                    //        }
                    //    }
                    //}
                    if (isValidPos)
                    {
                        FloorPiece floorHit = hit.collider.gameObject.GetComponent<FloorPiece>();
                        if (_currentPlacingModule.ModuleType == ModuleType.Garage)
                        {
                            _currentPlacingModule.SetWallActive(WallType.Front, hit.point.x > (_house.Length / 2f));
                            _currentPlacingModule.SetWallActive(WallType.Back, hit.point.x < (-_house.Length / 2f));
                            _currentPlacingModule.transform.SetParent(hit.transform);
                            _currentPlacingModule.transform.localPosition = Vector3.zero;
                        }
                        else
                        {
                            float x = hit.point.x - (hit.point.x % 0.5f);
                            float lengthMargin;
                            float widthMargin;
                            if (_currentPlacingModule.transform.rotation.eulerAngles.y % 180 < 1)
                            {
                                lengthMargin = ((floorHit.ParentFloor.Length / 2f) - (_currentPlacingModule.Length / 2f));
                                widthMargin = ((floorHit.ParentFloor.Width / 2f) - (_currentPlacingModule.Width / 2f));
                            }
                            else
                            {
                                lengthMargin = ((floorHit.ParentFloor.Length / 2f) - (_currentPlacingModule.Width / 2f));
                                widthMargin = ((floorHit.ParentFloor.Width / 2f) - (_currentPlacingModule.Length / 2f));
                            }
                            x = Mathf.Clamp(x, -lengthMargin+ floorHit.ParentFloor.transform.position.x, lengthMargin + floorHit.ParentFloor.transform.position.x);
                            float z = hit.point.z - (hit.point.z % 0.5f);
                            z = Mathf.Clamp(z, -widthMargin, widthMargin);
                            _currentPlacingModule.transform.SetParent(hit.transform.parent);
                            _currentPlacingModule.transform.position = new Vector3(x, hit.transform.parent.position.y, z);
                            CheckHideWalls(_currentPlacingModule, floorHit.ParentFloor.transform.position.x, lengthMargin, widthMargin);
                        }
                    }
                }
            }
            else if(_currentPlacingWindow)
            {
                Ray ray = _mainCam.ScreenPointToRay(Input.mousePosition);
                LayerMask mask = LayerMask.GetMask("OuterWall");
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 150, mask))
                {
                    Transform wallPieceTransform = hit.transform;
                    WallPiece newInsertPosition = wallPieceTransform.gameObject.GetComponent<WallPiece>();
                    if (newInsertPosition.GetIndexInParentWall() <= newInsertPosition.ParentWall.GetWallPieceCount() - _currentPlacingWindow.Width)
                    {
                        bool isAvailableForInsert = newInsertPosition.IsAvailableForInsert();
                        if(_currentPlacingWindow.Width > 1)
                        {
                            for (int i = 1; i < _currentPlacingWindow.Width; i++)
                            {
                                int nextIndex = newInsertPosition.GetIndexInParentWall() + i;
                                isAvailableForInsert &= newInsertPosition.ParentWall.GetWallPieceAtIndex(nextIndex).IsAvailableForInsert();
                            }
                        }
                        if (newInsertPosition && isAvailableForInsert)
                        {
                            _currentPlacingWindow.transform.SetParent(wallPieceTransform);
                            _currentPlacingWindow.transform.localPosition = Vector3.zero;
                            _currentPlacingWindow.transform.localRotation = Quaternion.identity;
                            if (newInsertPosition != _lastInsertWallPiece)
                            {
                                if (_lastInsertWallPiece)
                                {
                                    _lastInsertWallPiece.SetVisble(true);
                                    if(_currentPlacingWindow.Width > 1)
                                    {
                                        for (int i = 1; i < _currentPlacingWindow.Width; i++)
                                        {
                                            int nextIndex = _lastInsertWallPiece.GetIndexInParentWall() + i;
                                            _lastInsertWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(true);
                                        }
                                    }
                                }
                                _lastInsertWallPiece = newInsertPosition;
                            }
                        }
                        if (_lastInsertWallPiece)
                        {
                            _lastInsertWallPiece.SetVisble(false);
                            if (_currentPlacingWindow.Width > 1)
                            {
                                for (int i = 1; i < _currentPlacingWindow.Width; i++)
                                {
                                    int nextIndex = _lastInsertWallPiece.GetIndexInParentWall() + i;
                                    _lastInsertWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(false);
                                }
                            }
                        }
                    }
                }

            }
            else if (_currentPlacingDoor)
            {
                Ray ray = _mainCam.ScreenPointToRay(Input.mousePosition);
                LayerMask mask;
                if (_currentPlacingDoor.DoorLocationType == DoorLocationType.Inside)
                {
                    mask = LayerMask.GetMask("InnerWall");
                }
                else
                {
                    mask = LayerMask.GetMask("OuterWall");
                }
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 150, mask))
                {
                    Transform wallPieceTransform = hit.transform;
                    WallPiece newInsertPosition = wallPieceTransform.gameObject.GetComponent<WallPiece>();
                    if (newInsertPosition.GetIndexInParentWall() <= newInsertPosition.ParentWall.GetWallPieceCount() - _currentPlacingDoor.Width)
                    {
                        bool isAvailableForInsert = newInsertPosition.IsAvailableForInsert();
                        if (_currentPlacingDoor.Width > 1)
                        {
                            for (int i = 1; i < _currentPlacingDoor.Width; i++)
                            {
                                int nextIndex = newInsertPosition.GetIndexInParentWall() + i;
                                isAvailableForInsert &= newInsertPosition.ParentWall.GetWallPieceAtIndex(nextIndex).IsAvailableForInsert();
                            }
                        }
                        if (newInsertPosition && isAvailableForInsert)
                        {
                            _currentPlacingDoor.transform.SetParent(wallPieceTransform);
                            _currentPlacingDoor.transform.localPosition = Vector3.zero;
                            _currentPlacingDoor.transform.localRotation = Quaternion.identity;
                            if (newInsertPosition != _lastInsertWallPiece)
                            {
                                if (_lastInsertWallPiece)
                                {
                                    _lastInsertWallPiece.SetVisble(true);
                                    if (_currentPlacingDoor.Width > 1)
                                    {
                                        for (int i = 1; i < _currentPlacingDoor.Width; i++)
                                        {
                                            int nextIndex = _lastInsertWallPiece.GetIndexInParentWall() + i;
                                            _lastInsertWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(true);
                                        }
                                    }
                                }
                                _lastInsertWallPiece = newInsertPosition;
                            }
                        }
                        if (_lastInsertWallPiece)
                        {
                            _lastInsertWallPiece.SetVisble(false);
                            if (_currentPlacingDoor.Width > 1)
                            {
                                for (int i = 1; i < _currentPlacingDoor.Width; i++)
                                {
                                    int nextIndex = _lastInsertWallPiece.GetIndexInParentWall() + i;
                                    _lastInsertWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(false);
                                }
                            }
                        }
                    }
                }

            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            //Place current item if position is valid
            if (_currentPlacingModule)
            {
                if (_currentPlacingModule.IsValidPosition)
                {
                    _currentPlacingModule.SetPlaced(true);
                    if(_placedModules.Contains(_currentPlacingModule) == false)
                    {
                        _placedModules.Add(_currentPlacingModule);
                    }
                }
                else
                {
                    if(_placedModules.Contains(_currentPlacingModule))
                    {
                        _interactionPanel.SetActive(false);
                        _placedModules.Remove(_currentPlacingModule);
                    }
                    Destroy(_currentPlacingModule.gameObject);
                    for (int i = 0; i < _placedModules.Count; i++)
                    {
                        _placedModules[i].ClearOverlap();
                    }
                }
                _currentPlacingModule = null;
            }
            else if (_currentPlacingWindow)
            {
                if (_lastInsertWallPiece)
                {
                    //TODO add to json structure
                    _lastInsertWallPiece.SetInsert(_currentPlacingWindow.gameObject);
                    if (_currentPlacingWindow.Width > 1)
                    {
                        for (int i = 1; i < _currentPlacingWindow.Width; i++)
                        {
                            int nextIndex = _lastInsertWallPiece.GetIndexInParentWall() + i;
                            _lastInsertWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetInsert(_currentPlacingWindow.gameObject);
                        }
                    }
                    _currentPlacingWindow.ParentWallPiece = _lastInsertWallPiece;
                    if (_placedWindows.Contains(_currentPlacingWindow) == false)
                    {
                        _placedWindows.Add(_currentPlacingWindow);
                    }
                }
                else
                {
                    Destroy(_currentPlacingWindow.gameObject);
                }
                _currentPlacingWindow = null;
                _lastInsertWallPiece = null;
            }
            else if (_currentPlacingDoor)
            {
                if (_lastInsertWallPiece)
                {
                    //TODO add to json structure
                    _lastInsertWallPiece.SetInsert(_currentPlacingDoor.gameObject);
                    if (_currentPlacingDoor.Width > 1)
                    {
                        for (int i = 1; i < _currentPlacingDoor.Width; i++)
                        {
                            int nextIndex = _lastInsertWallPiece.GetIndexInParentWall() + i;
                            _lastInsertWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetInsert(_currentPlacingDoor.gameObject);
                        }
                    }
                    _currentPlacingDoor.ParentWallPiece = _lastInsertWallPiece;
                    if (_placedDoors.Contains(_currentPlacingDoor) == false)
                    {
                        _placedDoors.Add(_currentPlacingDoor);
                    }
                }
                else
                {
                    Destroy(_currentPlacingDoor.gameObject);
                }
                _currentPlacingDoor = null;
                _lastInsertWallPiece = null;
            }
            if (_isShowingTutorial == false)
            {
                _camera.SetShouldUpdate(true);
            }
        }
    }

    private void onInitialize(string sessionId, string serverUrl)
    {
        if(sessionId.Length > 0) LoadSession(sessionId);// Load session with this Id, else show default
        //serverUrl = url to use for API calls
    }


    private void CheckHideWalls(Room module, float parentfloorXpos, float lengthMargin, float widthMargin)
    {
        float x = module.transform.position.x;
        float z = module.transform.position.z;
        if (module.transform.rotation.eulerAngles.y % 360 < 1)
        {
            module.SetWallActive(WallType.Front, x  < lengthMargin + parentfloorXpos);
            module.SetWallActive(WallType.Back, x  > -lengthMargin + parentfloorXpos);
            module.SetWallActive(WallType.Left, z != -widthMargin);
            module.SetWallActive(WallType.Right, z != widthMargin);
        }
        else if (module.transform.rotation.eulerAngles.y % 360 < 91)
        {
            module.SetWallActive(WallType.Right, x < lengthMargin + parentfloorXpos);
            module.SetWallActive(WallType.Left, x > -lengthMargin + parentfloorXpos);
            module.SetWallActive(WallType.Front, z != -widthMargin);
            module.SetWallActive(WallType.Back, z != widthMargin);
        }
        else if (module.transform.rotation.eulerAngles.y % 360 < 181)
        {
            module.SetWallActive(WallType.Front, x > -lengthMargin + parentfloorXpos);
            module.SetWallActive(WallType.Back, x < lengthMargin + parentfloorXpos);
            module.SetWallActive(WallType.Left, z != widthMargin);
            module.SetWallActive(WallType.Right, z != -widthMargin);
        }
        else if (module.transform.rotation.eulerAngles.y % 360 < 271)
        {
            module.SetWallActive(WallType.Right, x > -lengthMargin + parentfloorXpos);
            module.SetWallActive(WallType.Left, x < lengthMargin + parentfloorXpos);
            module.SetWallActive(WallType.Front, z != widthMargin);
            module.SetWallActive(WallType.Back, z != -widthMargin);
        }
    }

    private void ScreenConfirmClicked(ScreenType type)
    {
        switch(type)
        {
            case ScreenType.SetSize:
                {
                    _currentScreen = ScreenType.SetContents;
                }
                break;
            case ScreenType.SetContents:
                {
                    _currentScreen = ScreenType.SetLabels;
                }
                break;
            case ScreenType.SetLabels:
                {
                    _setLabelsScreen.ClearInputFields();
                    _currentScreen = ScreenType.Overview;
                }
                break;
        }
        SetScreen(_currentScreen);
    }

    private void ScreenBackClicked(ScreenType type)
    {
        switch (type)
        {
            case ScreenType.SetContents:
                {
                    _currentScreen = ScreenType.SetSize;
                }
                break;
            case ScreenType.SetLabels:
                {
                    _setLabelsScreen.ClearInputFields();
                    _currentScreen = ScreenType.SetContents;
                }
                break;
            case ScreenType.Overview:
                {
                    _currentScreen = ScreenType.SetLabels;
                }
                break;
        }
        SetScreen(_currentScreen);
    }

    private void SetScreen(ScreenType type)
    {
        _setSizeScreen.SetActive(type == ScreenType.SetSize);
        _setContentsScreen.SetActive(type == ScreenType.SetContents);
        _setLabelsScreen.SetActive(type == ScreenType.SetLabels);
        _overviewScreen.SetActive(type == ScreenType.Overview);

        switch(type)
        {
            case ScreenType.SetLabels:
                {
                    _setLabelsScreen.ClearInputFields();
                    for (int i = 0; i < _placedModules.Count; i++)
                    {
                        Room module = _placedModules[i];
                        InputField inputField = _setLabelsScreen.GetInputField(module.GetName(), module.transform);
                        inputField.onValueChanged.AddListener((value) => { module.SetName(value); });
                    }
                }
                break;
            case ScreenType.Overview:
                {
                    _overviewScreen.ClearItems();
                    float surface = _house.Width * _house.Length;
                    for (int i = 0; i < _placedModules.Count; i++)
                    {
                        OverviewItem item = _overviewScreen.AddItem(OverviewItemType.Module, _placedModules[i].GetName());
                        item.SetDimensions(_placedModules[i].Width * 100, _placedModules[i].Length * 100);
                        item.SetImage(_placedModules[i].Image);
                        if (_placedModules[i].ModuleType == ModuleType.Garage)
                        {
                            surface += _placedModules[i].Length * _placedModules[i].Width;
                        }
                    }
                    _overviewScreen.SetTotalSurfaceText(surface);
                    for (int i = 0; i < _placedWindows.Count; i++)
                    {
                        OverviewItem item = _overviewScreen.AddItem(OverviewItemType.Window, _placedWindows[i].GetName());
                        item.SetDimensions(_placedWindows[i].WindowHeightInCm, _placedWindows[i].WindowWidthInCm);
                        item.SetImage(_placedWindows[i].Image);
                    }
                    for (int i = 0; i < _placedDoors.Count; i++)
                    {
                        if (_placedDoors[i].DoorType != DoorType.DoorOpenWallInside)
                        {
                            OverviewItem item = _overviewScreen.AddItem(OverviewItemType.Door, _placedDoors[i].GetName());
                            item.SetDimensions(_placedDoors[i].DoorHeightInCm, _placedDoors[i].DoorWidthInCm);
                            item.SetImage(_placedDoors[i].Image);
                            if (_placedDoors[i].ParentWallPiece.ParentWall.ParentRoom)
                            {
                                item.SetParentRoomName(_placedDoors[i].ParentWallPiece.ParentWall.ParentRoom.GetName());
                            }
                            else
                            {
                                item.SetParentRoomName(null);
                            }
                        }
                    }
                }
                break;
        }

    }

    private void OnMoveObject()
    {
        if (_currentSelectedModule)
        {
            _currentPlacingModule = _currentSelectedModule;
            _currentPlacingModule.SetPlaced(false);
        }
        if (_currentSelectedWindow)
        {
            _currentPlacingWindow = _currentSelectedWindow;
            _currentPlacingWindow.ParentWallPiece.SetInsert(null);
            _currentPlacingWindow.ParentWallPiece.SetVisble(true);
            if (_currentPlacingWindow.Width > 1)
            {
                for (int i = 1; i < _currentPlacingWindow.Width; i++)
                {
                    int nextIndex = _currentPlacingWindow.ParentWallPiece.GetIndexInParentWall() + i;
                    _currentPlacingWindow.ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetInsert(null);
                    _currentPlacingWindow.ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(true);
                }
            }
        }
        if (_currentSelectedDoor)
        {
            _currentPlacingDoor = _currentSelectedDoor;
            _currentPlacingDoor.ParentWallPiece.SetInsert(null);
            _currentPlacingDoor.ParentWallPiece.SetVisble(true);
            if (_currentPlacingDoor.Width > 1)
            {
                for (int i = 1; i < _currentPlacingDoor.Width; i++)
                {
                    int nextIndex = _currentPlacingDoor.ParentWallPiece.GetIndexInParentWall() + i;
                    _currentPlacingDoor.ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetInsert(null);
                    _currentPlacingDoor.ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(true);
                }
            }
        }
        _camera.SetShouldUpdate(false);
    }

    private void OnRotateObject()
    {
        if (_currentSelectedModule)
        {
            LayerMask mask = LayerMask.GetMask("Room");
            Collider[] overlappingRooms;

            float lengthMargin;
            float widthMargin;
            HouseFloor floor = _currentSelectedModule.transform.parent.GetComponent<HouseFloor>();
            if (_currentSelectedModule.transform.rotation.eulerAngles.y % 180 < 1)
            {
                overlappingRooms = Physics.OverlapBox(_currentSelectedModule.transform.position, new Vector3(_currentSelectedModule.Width / 2f, 0.5f, _currentSelectedModule.Length / 2f), Quaternion.identity, mask);
                lengthMargin = ((floor.Length / 2f) - (_currentSelectedModule.Length / 2f));
                widthMargin = ((floor.Width / 2f) - (_currentSelectedModule.Width / 2f));
            }
            else
            {
                overlappingRooms = Physics.OverlapBox(_currentSelectedModule.transform.position, new Vector3(_currentSelectedModule.Length / 2f, 0.5f, _currentSelectedModule.Width / 2f), Quaternion.identity, mask);
                lengthMargin = ((floor.Length / 2f) - (_currentSelectedModule.Width / 2f));
                widthMargin = ((floor.Width / 2f) - (_currentSelectedModule.Length / 2f));
            }
            if (overlappingRooms.Length > 1)
            {
                _currentSelectedModule.transform.Rotate(new Vector3(0, 180, 0));
                CheckHideWalls(_currentSelectedModule, floor.transform.position.x, lengthMargin, widthMargin);
                return;
            }
            mask = LayerMask.GetMask("OuterWall");
            if (_currentSelectedModule.transform.rotation.eulerAngles.y % 180 < 1)
            {
                overlappingRooms = Physics.OverlapBox(_currentSelectedModule.transform.position + Vector3.up, new Vector3(_currentSelectedModule.Width / 2f, 0.45f, _currentSelectedModule.Length / 2f), Quaternion.identity, mask);
            }
            else
            {
                overlappingRooms = Physics.OverlapBox(_currentSelectedModule.transform.position + Vector3.up, new Vector3(_currentSelectedModule.Length / 2f, 0.45f, _currentSelectedModule.Width / 2f), Quaternion.identity, mask);
            }
            if (overlappingRooms.Length > 1)
            {
                _currentSelectedModule.transform.Rotate(new Vector3(0, 180, 0));
                if (_currentSelectedModule.transform.rotation.eulerAngles.y % 180 < 1)
                {
                    lengthMargin = ((floor.Length / 2f) - (_currentSelectedModule.Length / 2f));
                    widthMargin = ((floor.Width / 2f) - (_currentSelectedModule.Width / 2f));
                }
                else
                {
                    lengthMargin = ((floor.Length / 2f) - (_currentSelectedModule.Width / 2f));
                    widthMargin = ((floor.Width / 2f) - (_currentSelectedModule.Length / 2f));
                }
                CheckHideWalls(_currentSelectedModule, floor.transform.position.x, lengthMargin, widthMargin);
                return;
            }
            _currentSelectedModule.transform.Rotate(new Vector3(0, 90, 0));
            if (_currentSelectedModule.transform.rotation.eulerAngles.y % 180 < 1)
            {
                lengthMargin = ((floor.Length / 2f) - (_currentSelectedModule.Length / 2f));
                widthMargin = ((floor.Width / 2f) - (_currentSelectedModule.Width / 2f));
            }
            else
            {
                lengthMargin = ((floor.Length / 2f) - (_currentSelectedModule.Width / 2f));
                widthMargin = ((floor.Width / 2f) - (_currentSelectedModule.Length / 2f));
            }
            CheckHideWalls(_currentSelectedModule, floor.transform.position.x, lengthMargin, widthMargin);
        }
    }

    private void OnDeleteObject()
    {
        if (_currentSelectedModule)
        {
            _placedModules.Remove(_placedModules.Find(module => module.Id == _currentSelectedModule.Id));
            Destroy(_currentSelectedModule.gameObject);
            _currentSelectedModule = null;
        }
        if (_currentSelectedWindow)
        {
            _placedWindows.Remove(_placedWindows.Find(window => window.Id == _currentSelectedWindow.Id));
            _currentSelectedWindow.ParentWallPiece.SetInsert(null);
            _currentSelectedWindow.ParentWallPiece.SetVisble(true);
            if (_currentSelectedWindow.Width > 1)
            {
                for (int i = 1; i < _currentSelectedWindow.Width; i++)
                {
                    int nextIndex = _currentSelectedWindow.ParentWallPiece.GetIndexInParentWall() + i;
                    _currentSelectedWindow.ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetInsert(null);
                    _currentSelectedWindow.ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(true);
                }
            }
            Destroy(_currentSelectedWindow.gameObject);
            _currentSelectedWindow = null;
        }
        if (_currentSelectedDoor)
        {
            _placedDoors.Remove(_placedDoors.Find(window => window.Id == _currentSelectedDoor.Id));
            _currentSelectedDoor.ParentWallPiece.SetInsert(null);
            _currentSelectedDoor.ParentWallPiece.SetVisble(true);
            if (_currentSelectedDoor.Width > 1)
            {
                for (int i = 1; i < _currentSelectedDoor.Width; i++)
                {
                    int nextIndex = _currentSelectedDoor.ParentWallPiece.GetIndexInParentWall() + i;
                    _currentSelectedDoor.ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetInsert(null);
                    _currentSelectedDoor.ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(true);
                }
            }
            Destroy(_currentSelectedDoor.gameObject);
            _currentSelectedDoor = null;
        }
        _interactionPanel.SetActive(false);
    }

    private void OnReset()
    {
        _confirmUI.ConfirmType = ConfirmType.ClearHouse;
        _confirmUI.SetText("Are you sure you want to clear everything?");
        _confirmUI.SetActive(true);
    }

    private void OnRecenter()
    {
        _camera.Recenter();
    }

    private void OnShowMeasurements()
    {
        _showMeasurements = !_showMeasurements;
        _house.SetMeasureLinesActive(_showMeasurements);
    }

    private void OnShowTutorial()
    {
        _tutorialScreen.SetActive(true);
        _isShowingTutorial = true;
        _camera.SetShouldUpdate(false);
    }

    private void CloseTutorial()
    {
        _tutorialScreen.SetActive(false);
        _isShowingTutorial = false;
        _camera.SetShouldUpdate(true);
    }

    private void OnShowContactScreen()
    {
        _contactScreen.SetActive(true);
        _isShowingTutorial = true;
        _camera.SetShouldUpdate(false);
    }

    private void OnConfirm()
    {
        switch (_confirmUI.ConfirmType)
        {
            case ConfirmType.ClearHouse:
                {
                    ClearHouse();
                }
                break;
        }
        _confirmUI.SetActive(false);
    }

    private void OnCancel()
    {
        _confirmUI.SetActive(false);
    }

    private void CloseContactScreen()
    {
        _contactScreen.SetActive(false);
        _isShowingTutorial = false;
        _camera.SetShouldUpdate(true);
    }

    private void ConfirmCode()
    {
        string code = _codeScreen.GetCode();
        LoadSession(code);
    }

    private void CloseCodeScreen()
    {
        _codeScreen.SetActive(false);
    }

    private void EnterCodeClicked()
    {
#if !UNITY_EDITOR
        _codeScreen.SetActive(true);
        _codeScreen.SetCOnfirmButtonActive(true);

#else
        //LoadConfiguration
        
        ReceiveMessage("{\"data\":{\"Width\":8.0,\"Length\":8.0,\"FirstFloorType\":1,\"Modules\":[{\"Id\":0,\"TypeId\":0,\"Floor\":0,\"Width\":5.0,\"Length\":3.0,\"Transform\":{\"x\":-2.5,\"y\":0.0,\"z\":1.5,\"rotation\":0.0},\"Label\":\"Kitchen_0\"},{\"Id\":1,\"TypeId\":1,\"Floor\":0,\"Width\":5.0,\"Length\":3.0,\"Transform\":{\"x\":2.5,\"y\":0.0,\"z\":-1.5,\"rotation\":0.0},\"Label\":\"Bedroom 1_1\"},{\"Id\":2,\"TypeId\":3,\"Floor\":0,\"Width\":3.0,\"Length\":3.0,\"Transform\":{\"x\":2.5,\"y\":0.0,\"z\":2.5,\"rotation\":0.0},\"Label\":\"Bedroom 3_2\"},{\"Id\":3,\"TypeId\":6,\"Floor\":0,\"Width\":8.0,\"Length\":4.0,\"Transform\":{\"x\":-6.0,\"y\":0.0,\"z\":0.0,\"rotation\":0.0},\"Label\":\"Garage_0\"},{\"Id\":4,\"TypeId\":7,\"Floor\":0,\"Width\":2.0,\"Length\":4.0,\"Transform\":{\"x\":-2.0,\"y\":0.0,\"z\":-2.0,\"rotation\":0.0},\"Label\":\"Hall_4\"},{\"Id\":5,\"TypeId\":9,\"Floor\":0,\"Width\":5.0,\"Length\":3.0,\"Transform\":{\"x\":2.5,\"y\":2.85,\"z\":1.5,\"rotation\":0.0},\"Label\":\"Dining room_5\"},{\"Id\":6,\"TypeId\":11,\"Floor\":0,\"Width\":2.0,\"Length\":1.0,\"Transform\":{\"x\":3.5,\"y\":2.85,\"z\":-2.0,\"rotation\":0.0},\"Label\":\"Toilet_6\"},{\"Id\":7,\"TypeId\":10,\"Floor\":0,\"Width\":2.0,\"Length\":2.0,\"Transform\":{\"x\":-6.5,\"y\":2.85,\"z\":-3.0,\"rotation\":0.0},\"Label\":\"Storage_7\"}],\"Windows\":[{\"Id\":0,\"TypeId\":1,\"Floor\":0,\"Wall\":3,\"WallPieceIndex\":1},{\"Id\":1,\"TypeId\":0,\"Floor\":0,\"Wall\":2,\"WallPieceIndex\":6},{\"Id\":2,\"TypeId\":3,\"Floor\":0,\"Wall\":0,\"WallPieceIndex\":5},{\"Id\":3,\"TypeId\":0,\"Floor\":1,\"Wall\":2,\"WallPieceIndex\":6},{\"Id\":4,\"TypeId\":1,\"Floor\":1,\"Wall\":2,\"WallPieceIndex\":4}],\"Doors\":[{\"Id\":1,\"TypeId\":14,\"Floor\":0,\"RoomId\":4,\"Wall\":3,\"WallPieceIndex\":3,\"IsOutside\":false},{\"Id\":2,\"TypeId\":15,\"Floor\":0,\"RoomId\":2,\"Wall\":1,\"WallPieceIndex\":0,\"IsOutside\":false},{\"Id\":3,\"TypeId\":2,\"Floor\":0,\"RoomId\":-1,\"Wall\":1,\"WallPieceIndex\":3,\"IsOutside\":true},{\"Id\":4,\"TypeId\":3,\"Floor\":0,\"RoomId\":-1,\"Wall\":2,\"WallPieceIndex\":3,\"IsOutside\":true}]},\"type\":\"LoadConfiguration\"}");
#endif
    }

    private string _requestedSessionId = "";
    void LoadSession(string sessionId)
    {
        _requestedSessionId = sessionId;
        StartCoroutine(SessionService.GetSession(sessionId, OnSessionReceived));
    }

    void OnSessionReceived(GetSessionResponse response)
    {
        if (!response.IsSuccess())
        {
            Debug.LogError("GetSession failed, reason: " + response.error);
        }
        else
        {
            Debug.Assert(response.session != null);
            if(response.session != null)
            {
                Debug.Assert(_requestedSessionId.Length > 0);
                _sessionId = _requestedSessionId;
                _requestedSessionId = "";
                LoadData(response.session.data);

                Debug.Log("Session has been loaded");
            }
        }
    }

    private void OnWidthChanged(float newWidth)
    {
        ClearHouse();
        _house.SetWidth(newWidth);
    }

    private void OnLengthChanged(float newWLength)
    {
        ClearHouse();
        _house.SetLength(newWLength);
        OnExtendFirstFloorChanged(_firstFloorType);
    }

    private void OnExtendFirstFloorChanged(int newValue)
    {
        _firstFloorType = newValue;
        switch (newValue)
        {
            case 0:
                {
                    Vector3 pos = _house.GetFloor(1).transform.localPosition;
                    pos.x = 0;
                    _house.GetFloor(1).transform.localPosition = pos;
                    _house.GetFloor(1).SetLength(_house.Length);
                }
                break;
            case 1:
                {
                    Vector3 pos = _house.GetFloor(1).transform.localPosition;
                    pos.x = -2;
                    _house.GetFloor(1).transform.localPosition = pos;
                    _house.GetFloor(1).SetLength(_house.Length + 4);
                }
                break;
            case 2:
                {
                    Vector3 pos = _house.GetFloor(1).transform.localPosition;
                    pos.x = 2;
                    _house.GetFloor(1).transform.localPosition = pos;
                    _house.GetFloor(1).SetLength(_house.Length + 4);
                }
                break;
            case 3:
                {
                    Vector3 pos = _house.GetFloor(1).transform.localPosition;
                    pos.x = 0;
                    _house.GetFloor(1).transform.localPosition = pos;
                    _house.GetFloor(1).SetLength(_house.Length + 8);
                }
                break;
        }
    }


    private void ReceiveMessage(string messageJson)
    {
        Message message = JsonConvert.DeserializeObject<Message>(messageJson, new JsonSerializerSettings
        {
            MissingMemberHandling = MissingMemberHandling.Ignore
        });
        if (message.type == "LoadConfiguration")
        {
            SaveDataMessage saveMessage = JsonConvert.DeserializeObject<SaveDataMessage>(messageJson, new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Ignore
            });
            LoadData(saveMessage.data);
        }
        else if(message.type == "ReceivedCode")
        {
            CodeMessage saveMessage = JsonConvert.DeserializeObject<CodeMessage>(messageJson, new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Ignore
            });
            _codeScreen.SetCode(saveMessage.code);
            _codeScreen.SetActive(true);
            _codeScreen.SetCOnfirmButtonActive(false);
        }
    }

    private void GetCode()
    {
        HouseData data = new HouseData();
        data.Length = _house.Length;
        data.Width = _house.Width;
        data.FirstFloorType = _firstFloorType;
        data.Modules = new List<ModuleData>();
        for (int i = 0; i < _placedModules.Count; i++)
        {
            ModuleData module = new ModuleData()
            {
                Id = _placedModules[i].Id,
                Label = _placedModules[i].GetName(),
                Length = _placedModules[i].Length,
                Width = _placedModules[i].Width,
                TypeId = (int)_placedModules[i].ModuleType,
                Transform = new TransformData()
                {
                    rotation = _placedModules[i].transform.rotation.eulerAngles.y,
                    x = _placedModules[i].transform.position.x,
                    y = _placedModules[i].transform.position.y,
                    z = _placedModules[i].transform.position.z,
                }
            };
            data.Modules.Add(module);
        }
        data.Windows = new List<WindowData>();
        for (int i = 0; i < _placedWindows.Count; i++)
        {
            WindowData window = new WindowData()
            {
                Id = _placedWindows[i].Id,
                TypeId = (int)_placedWindows[i].WindowType,
                Wall = (int)_placedWindows[i].ParentWallPiece.ParentWall.WallType,
                WallPieceIndex = _placedWindows[i].ParentWallPiece.GetIndexInParentWall(),
                Floor = _placedWindows[i].ParentWallPiece.transform.position.y > 0 ? 1 : 0
            };
            data.Windows.Add(window);
        }
        data.Doors = new List<DoorData>();
        for (int i = 0; i < _placedDoors.Count; i++)
        {
            DoorData door = new DoorData()
            {
                Id = _placedDoors[i].Id,
                TypeId = (int)_placedDoors[i].DoorType,
                Wall = (int)_placedDoors[i].ParentWallPiece.ParentWall.WallType,
                WallPieceIndex = _placedDoors[i].ParentWallPiece.GetIndexInParentWall(),
                Floor = _placedDoors[i].ParentWallPiece.transform.position.y > 0 ? 1 : 0,
                IsOutside = _placedDoors[i].DoorLocationType == DoorLocationType.Outside
            };
            if (_placedDoors[i].ParentWallPiece.ParentWall.ParentRoom)
            {
                door.RoomId = _placedDoors[i].ParentWallPiece.ParentWall.ParentRoom.Id;
            }
            else
            {
                door.RoomId = -1;
            }
            if (_placedDoors[i].ParentWallPiece.ParentWall.ParentRoom)
            {
                door.RoomId = _placedDoors[i].ParentWallPiece.ParentWall.ParentRoom.Id;
            }
            data.Doors.Add(door);
        }


        Session session = new Session();
        session.data = data;

        //string json = SerializationHelper.SerializeObject(data);
        //Debug.Log(json);

        if(!IsWorkingInSession()) CreateSession(session);
        else UpdateSession(_sessionId, session);
    }

    void CreateSession(Session session)
    {
        StartCoroutine(SessionService.CreateSession(session, OnSessionCreatedOrUpdated));
    }

    void UpdateSession(string sessionId, Session session)
    {
        StartCoroutine(SessionService.UpdateSession(sessionId, session, OnSessionCreatedOrUpdated));
    }

    void OnSessionCreatedOrUpdated(SessionResponse response)
    {
        if (!response.IsSuccess())
        {
            Debug.LogError("GetSession failed, reason: " + response.error);
        }
        else
        {
            Debug.Assert(response.id.Length > 0);
            _sessionId = response.id;
            _codeScreen.SetCode(_sessionId);
            _codeScreen.SetActive(true);
            _codeScreen.SetCOnfirmButtonActive(false);
            Debug.Log("Session has been saved");
        }
    }

    private void DownloadClicked()
    {
        SaveAndDownloadPDF.CreatePDF(TakeScreenschots(), _house.Width, _house.Length, _placedModules, _placedWindows, _placedDoors);
    }

    private void LoadData(HouseData data)
    {
        ClearHouse();
        _house.SetLength(data.Length);
        _house.SetWidth(data.Width);
        OnExtendFirstFloorChanged(data.FirstFloorType);
        for (int i = 0; i < data.Modules.Count; i++)
        {
            ModuleData module = data.Modules[i];
            SelectModule(module.TypeId);
            _currentPlacingModule.Initialize();
            _currentPlacingModule.Id = module.Id;
            _currentPlacingModule.SetName(module.Label);
            if (_currentPlacingModule.ModuleType == ModuleType.Garage)
            {
                _currentPlacingModule.SetWidth(_house.Width);
                _currentPlacingModule.SetLength(4f);
                _currentPlacingModule.SetWallActive(WallType.Front, module.Transform.x > (_house.Length / 2f));
                _currentPlacingModule.SetWallActive(WallType.Back, module.Transform.x < (-_house.Length / 2f));                
                _currentPlacingModule.transform.SetParent(_house.GetGaragePlaceholder(module.Transform.x > (_house.Length / 2f)));
                _currentPlacingModule.transform.localPosition = Vector3.zero;
            }
            else
            {
                _currentPlacingModule.SetLength(module.Length);
                _currentPlacingModule.SetWidth(module.Width);
                HouseFloor parentFloor = null;
                if (Mathf.Floor(module.Transform.y) == 0)
                {
                    parentFloor = _house.GetFloor(0);
                }
                else
                {
                    parentFloor = _house.GetFloor(1);
                }
                float x = module.Transform.x;
                float lengthMargin;
                float widthMargin;
                if (_currentPlacingModule.transform.rotation.eulerAngles.y % 180 < 1)
                {
                    lengthMargin = ((parentFloor.Length / 2f) - (_currentPlacingModule.Length / 2f));
                    widthMargin = ((parentFloor.Width / 2f) - (_currentPlacingModule.Width / 2f));
                }
                else
                {
                    lengthMargin = ((parentFloor.Length / 2f) - (_currentPlacingModule.Width / 2f));
                    widthMargin = ((parentFloor.Width / 2f) - (_currentPlacingModule.Length / 2f));
                }
                x = Mathf.Clamp(x, -lengthMargin + parentFloor.transform.position.x, lengthMargin + parentFloor.transform.position.x);
                _currentPlacingModule.SetWallActive(WallType.Front, x != lengthMargin);
                _currentPlacingModule.SetWallActive(WallType.Back, x != -lengthMargin);
                float z = module.Transform.z;
                z = Mathf.Clamp(z, -widthMargin, widthMargin);
                _currentPlacingModule.SetWallActive(WallType.Left, z != -widthMargin);
                _currentPlacingModule.SetWallActive(WallType.Right, z != widthMargin);
                _currentPlacingModule.transform.SetParent(parentFloor.transform);
                _currentPlacingModule.transform.position = new Vector3(x, module.Transform.y, z);
            }
            Vector3 angles = _currentPlacingModule.transform.rotation.eulerAngles;
            angles.y = module.Transform.rotation;
            _currentPlacingModule.transform.rotation = Quaternion.Euler(angles);
            _currentPlacingModule.SetPlaced(true);
            _placedModules.Add(_currentPlacingModule);
            _currentPlacingModule = null;
        }

        for (int i = 0; i < data.Windows.Count; i++)
        {
            WindowData window = data.Windows[i];
            SelectWindow(window.TypeId);
            _currentPlacingWindow.Id = window.Id;
            WallPiece parent = _house.GetFloor(window.Floor).GetWall((WallType)window.Wall).GetWallPieceAtIndex(window.WallPieceIndex);
            _currentPlacingWindow.ParentWallPiece = parent;
            _currentPlacingWindow.transform.SetParent(parent.transform);
            _currentPlacingWindow.transform.localPosition = Vector3.zero;
            _currentPlacingWindow.transform.localRotation = Quaternion.identity;
            parent.SetVisble(false);
            parent.SetInsert(_currentPlacingWindow.gameObject);
            if (_currentPlacingWindow.Width > 1)
            {
                for (int j = 1; j < _currentPlacingWindow.Width; j++)
                {
                    int nextIndex = window.WallPieceIndex + j;
                    parent.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(false);
                    parent.ParentWall.GetWallPieceAtIndex(nextIndex).SetInsert(_currentPlacingWindow.gameObject);
                }
            }
            _placedWindows.Add(_currentPlacingWindow);
            _currentPlacingWindow = null;
        }

        for (int i = 0; i < data.Doors.Count; i++)
        {
            DoorData door = data.Doors[i];
            if (door.IsOutside)
            {
                SelectOutsideDoor(door.TypeId);
            }
            else
            {
                DoorType type = (DoorType)door.TypeId;
                int prefabId = 0;
                switch (type)
                {
                    case DoorType.DoorInside:
                        {
                            prefabId = 0;
                        }
                        break;
                    case DoorType.DoorOpenWallInside:
                        {
                            prefabId = 1;
                        }
                        break;
                }
                SelectInsideDoor(prefabId);
            }
            _currentPlacingDoor.Id = door.Id;
            WallPiece parent = null;
            if (door.RoomId == -1)
            {
                parent = _house.GetFloor(door.Floor).GetWall((WallType)door.Wall).GetWallPieceAtIndex(door.WallPieceIndex);
            }
            else
            {
                Room parentRoom = _placedModules.Find(room => room.Id == door.RoomId);
                parent = parentRoom.GetWall((WallType)door.Wall).GetWallPieceAtIndex(door.WallPieceIndex);
            }
            _currentPlacingDoor.ParentWallPiece = parent;
            _currentPlacingDoor.transform.SetParent(parent.transform);
            _currentPlacingDoor.transform.localPosition = Vector3.zero;
            _currentPlacingDoor.transform.localRotation = Quaternion.identity;
            parent.SetVisble(false);
            parent.SetInsert(_currentPlacingDoor.gameObject);
            if (_currentPlacingDoor.Width > 1)
            {
                for (int j = 1; j < _currentPlacingDoor.Width; j++)
                {
                    int nextIndex = door.WallPieceIndex + j;
                    parent.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(false);
                    parent.ParentWall.GetWallPieceAtIndex(nextIndex).SetInsert(_currentPlacingDoor.gameObject);
                }
            }
            _placedDoors.Add(_currentPlacingDoor);
            _currentPlacingDoor = null;
        }
    }

    private void ClearHouse()
    {
        for (int i = 0; i < _placedWindows.Count; i++)
        {
            _placedWindows[i].ParentWallPiece.SetVisble(true);
            _placedWindows[i].ParentWallPiece.SetInsert(null);
            if (_placedWindows[i].Width > 1)
            {
                for (int j = 1; j < _placedWindows[i].Width; j++)
                {
                    int nextIndex = _placedWindows[i].ParentWallPiece.GetIndexInParentWall() + j;
                    _placedWindows[i].ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetInsert(null);
                    _placedWindows[i].ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(true);
                }
            }
            Destroy(_placedWindows[i].gameObject);
        }
        _placedWindows.Clear();
        for (int i = 0; i < _placedDoors.Count; i++)
        {
            _placedDoors[i].ParentWallPiece.SetVisble(true);
            _placedDoors[i].ParentWallPiece.SetInsert(null);
            if (_placedDoors[i].Width > 1)
            {
                for (int j = 1; j < _placedDoors[i].Width; j++)
                {
                    int nextIndex = _placedDoors[i].ParentWallPiece.GetIndexInParentWall() + j;
                    _placedDoors[i].ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetInsert(null);
                    _placedDoors[i].ParentWallPiece.ParentWall.GetWallPieceAtIndex(nextIndex).SetVisble(true);
                }
            }
            Destroy(_placedDoors[i].gameObject);
        }
        _placedDoors.Clear();
        for(int i = 0; i <  _placedModules.Count; i++)
        {
            Destroy(_placedModules[i].gameObject);
        }
        _placedModules.Clear();
    }

    private void OnSelectedFloorChanged(bool firstFloorActive)
    {
        _house.SetFirstFloorActive(firstFloorActive);
    }

    private void SelectModule(int id)
    {
        if (_currentPlacingModule == null)
        {
            _currentPlacingModule = Instantiate(_modulePrefabs[id]);
            _camera.SetShouldUpdate(false);
            if (_currentPlacingModule.ModuleType == ModuleType.Garage)
            {
                _currentPlacingModule.Initialize();
                _currentPlacingModule.SetWidth( _house.Width);
                _currentPlacingModule.SetLength(4f);
            }
        }
    }

    private void SelectWindow(int id)
    {
        if (_currentPlacingWindow == null)
        {
            _currentPlacingWindow = Instantiate(_windowPrefabs[id]);
            _camera.SetShouldUpdate(false);
        }
    }

    private void SelectOutsideDoor(int id)
    {
        if (_currentPlacingDoor == null)
        {
            _currentPlacingDoor = Instantiate(_doorPrefabs[id]);
            _camera.SetShouldUpdate(false);
        }
    }

    private void SelectInsideDoor(int id)
    {
        if (_currentPlacingDoor == null)
        {
            _currentPlacingDoor = Instantiate(_insdeDoorPrefabs[id]);
            _camera.SetShouldUpdate(false);
        }
    }

    private Texture2D[] TakeScreenschots()
    {
        Texture2D[] screenshots = new Texture2D[2];
        bool firstFloorActive = _house.GetFloor(1).gameObject.activeSelf;
        _house.SetFirstFloorActive(false);
        _light.shadows = LightShadows.None;
        for (int i = 0; i < 2; i++)
        {
            _screenshotCamera.targetTexture = _rendertextures[i];
            _screenshotCamera.Render();
            screenshots[i] = new Texture2D(_rendertextures[i].width, _rendertextures[i].height, TextureFormat.RGB24, false);
            RenderTexture.active = _rendertextures[i];
            screenshots[i].ReadPixels(new Rect(0, 0, _rendertextures[i].width, _rendertextures[i].height), 0, 0);
            screenshots[i].Apply();
            RenderTexture.active = null;
            _house.SetFirstFloorActive(true);
        }
        _house.SetFirstFloorActive(firstFloorActive);
        _light.shadows = LightShadows.Soft;
        return screenshots;
    }
}
