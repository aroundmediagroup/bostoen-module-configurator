using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MeasureLine : MonoBehaviour
{
    [SerializeField] private Transform _line;
    [SerializeField] private TMP_Text _text;
    [SerializeField] private Transform _connectionLineLeft;
    [SerializeField] private Transform _connectionLineRight;

    public void SetSizeInM(float meters)
    {
        _text.text = (meters * 100) + " cm";
        Vector3 scale = _line.localScale;
        scale.x = meters;
        _line.localScale = scale;
        Vector3 pos = _connectionLineLeft.transform.localPosition;
        pos.x = meters / 2f;
        _connectionLineLeft.transform.localPosition = pos; 
        
        pos = _connectionLineRight.transform.localPosition;
        pos.x = -meters / 2f;
        _connectionLineRight.transform.localPosition = pos;
    }

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive)
;    }
}
