using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallPiece : MonoBehaviour
{
    [SerializeField] private Renderer _visual;

    public Wall ParentWall = null;

    private GameObject _insert = null;

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    public void SetVisble(bool isVisible)
    {
        _visual.enabled = isVisible;
    }

    public void SetInsert(GameObject insert)
    {
        _insert = insert;
    }

    public bool IsAvailableForInsert()
    {
        return _insert == null;
    }

    public int GetIndexInParentWall()
    {
        return ParentWall.GetWallPieceIndex(this);
    }
}
