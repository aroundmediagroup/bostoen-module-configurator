using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorPiece : MonoBehaviour
{
    [SerializeField] private Renderer _visual;

    public bool IsBlocked
    {
        get;
        private set;
    }

    public HouseFloor ParentFloor = null;

    private int _stairCount = 0;
    private int _roomCount = 0;

    private void OnEnable()
    {
        SetVisble(true);
    }

    public void SetVisble(bool isVisible)
    {
        _visual.enabled = isVisible;
    }

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            _stairCount++;
            if (_stairCount > 0)
            {
                IsBlocked = true;
                SetVisble(false);
            }
        }
        if (other.gameObject.layer == 9)
        {
            _roomCount++;
            IsBlocked = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            _stairCount--;
            if (_stairCount == 0)
            {
                IsBlocked = false;
                SetVisble(true);
            }
        }
        if (other.gameObject.layer == 9)
        {
            _roomCount--;
            if (_roomCount == 0)
            {
                IsBlocked = false;
            }
        }
    }
}
