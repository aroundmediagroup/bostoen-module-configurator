using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DoorLocationType
{
    Inside = 0,
    Outside = 1,

    Undefined = -1
}
public enum DoorType
{
    Door20 = 0,
    Door21 = 1,
    Door21Bis = 2,
    Door22 = 3,
    Door23 = 4,
    Door23Bis = 5,
    Door24 = 6,
    Door25 = 7,
    Door26 = 8,
    Door27 = 9,
    Door28 = 10,
    Door29 = 11,
    Door30 = 12,
    Door31 = 13,
    DoorInside = 14,
    DoorOpenWallInside = 15,

    Undefined = -1
}

public class Door : MonoBehaviour
{
    [HideInInspector] public WallPiece ParentWallPiece;
    [SerializeField] private DoorType _doorType = DoorType.Undefined;
    [SerializeField] private DoorLocationType _doorLocationType = DoorLocationType.Undefined;
    [SerializeField] private Sprite _overviewImage;
    [SerializeField] private string _overviewName = "Door";
    [SerializeField] private int _width = 1;
    public int DoorWidthInCm = 50;
    public int DoorHeightInCm = 50;

    public DoorType DoorType
    {
        get
        {
            return _doorType;
        }
    }
    public DoorLocationType DoorLocationType
    {
        get
        {
            return _doorLocationType;
        }
    }
    public Sprite Image
    {
        get
        {
            return _overviewImage;
        }
    }
    public int Width
    {
        get
        {
            return _width;
        }
    }


    static int lastId = 0;
    public int Id
    {
        get
        {
            return _id;
        }
        set
        {
            lastId = value;
            _id = lastId++;
            _isIdOverridden = true;
        }
    }
    private int _id;
    bool _isIdOverridden = false;

    private void Start()
    {
        if (_isIdOverridden == false)
        {
            _id = lastId++;
        }

    }

    public string GetName()
    {
        return _overviewName;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8 && DoorLocationType == DoorLocationType.Inside)
        {
            WallPiece wallPiece = other.gameObject.GetComponent<WallPiece>();
            if (wallPiece)
            {
                wallPiece.SetVisble(false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8 && DoorLocationType == DoorLocationType.Inside)
        {
            WallPiece wallPiece = other.gameObject.GetComponent<WallPiece>();
            if(wallPiece)
            {
                wallPiece.SetVisble(true);
            }    

        }
    }
}
