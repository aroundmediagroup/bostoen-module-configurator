using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class SaveDataMessage: Message
{
    public HouseData data;
}

[Serializable]
public class CodeMessage : Message
{
    public string code;
}

[Serializable]
public class HouseData
{
    public float Width;
    public float Length;
    public int FirstFloorType;
    public List<ModuleData> Modules;
    public List<WindowData> Windows;
    public List<DoorData> Doors;
}

[Serializable]
public class ModuleData
{
    public int Id;
    public int TypeId;
    public int Floor;
    public float Width;
    public float Length;
    public TransformData Transform;
    public string Label;
}

[Serializable]
public class WindowData
{
    public int Id;
    public int TypeId;
    public int Floor;
    public int Wall;
    public int WallPieceIndex;
}

[Serializable]
public class DoorData
{
    public int Id;
    public int TypeId;
    public int Floor;
    public int RoomId;
    public int Wall;
    public int WallPieceIndex;
    public bool IsOutside;
}

[Serializable]
public class TransformData
{
    public float x;
    public float y;
    public float z;
    public float rotation;
}