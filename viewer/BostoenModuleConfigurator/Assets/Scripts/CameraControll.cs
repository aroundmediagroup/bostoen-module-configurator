using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraControll : MonoBehaviour
{
    [SerializeField] private float _minDistance = 5;
    [SerializeField] private float _maxDistance = 15;
    [SerializeField] private float _minAngle = 5;
    [SerializeField] private float _maxAngle = 90;
    [SerializeField] private float _rotationSpeed = 25;
    [SerializeField] private float _MoveSpeed = 5;
    [SerializeField] private float _zoomSpeed = 25;

    private Transform _target;
    private float _distance = 11;
    private Vector3 _prevMousePos = Vector3.zero;
    private bool _shouldUpdate = true;
    private bool _isSlider;

    void Start()
    {
        _target = transform.parent;
        Recenter();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _isSlider = false;
            PointerEventData eventData = new PointerEventData(EventSystem.current);
            eventData.position = Input.mousePosition;
            List<RaycastResult> raysastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, raysastResults);
            for(int i = 0; i < raysastResults.Count; i++)
            {
                if(raysastResults[i].gameObject.CompareTag("Slider"))
                {
                    _isSlider = true;
                    break;
                }
            }
            _prevMousePos = Input.mousePosition;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            _isSlider = false;
        }
        if (_shouldUpdate && _isSlider == false)
        {
            if (Input.GetMouseButton(0))
            {
                //transform.parent.Rotate(Vector3.up, delta * _rotationSpeed * Time.deltaTime);
                float delta = Input.mousePosition.x - _prevMousePos.x;
                Vector3 euler = _target.rotation.eulerAngles;
                euler.y += delta * _rotationSpeed * Time.deltaTime;

                delta = Input.mousePosition.y - _prevMousePos.y;
                euler.x += -delta * _rotationSpeed * Time.deltaTime;
                euler.x = ClampAngle(euler.x, _minAngle, _maxAngle);
                _target.rotation = Quaternion.Euler(euler);
                _prevMousePos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(1))
            {
                Vector3 delta = Input.mousePosition - _prevMousePos;
                _target.transform.position += ((-delta.x * transform.right) + (-delta.y * Vector3.up)) * _MoveSpeed * Time.deltaTime;
                _prevMousePos = Input.mousePosition;
            }
            _prevMousePos = Input.mousePosition;


            transform.LookAt(_target);

            if (Input.mouseScrollDelta.sqrMagnitude != 0)
            {
                _distance -= Input.mouseScrollDelta.y * _zoomSpeed * Time.deltaTime;
                _distance = Mathf.Clamp(_distance, _minDistance, _maxDistance);
                AdjustDistance();
            }
        }
    }

    public void Recenter()
    {
        _target.position = Vector3.zero;
        _distance = 11;
        AdjustDistance();
    }

    public void SetShouldUpdate(bool shouldUpdate)
    {
        _shouldUpdate = shouldUpdate;
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360f)
            angle += 360f;
        if (angle > 360f)
            angle -= 360f;

        return Mathf.Clamp(angle, min, max);
    }

    private void AdjustDistance()
    {
        Vector3 pos = transform.localPosition;
        pos.z = -_distance;
        transform.localPosition = pos;
    }
}
