﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

public class WebPageInteractor : MonoBehaviour
{
    public static void ReceiveMessage(string messageJson)
    {
        Debug.Log(messageJson);
        if (OnMessageReceived != null) OnMessageReceived.Invoke(messageJson);
    }
    public void ReceiveMessageFromJavascript(string messageJson)
    {
        ReceiveMessage(messageJson);
    }

    public delegate void OnMessageReceivedDelegate(string messageJson);
    public static OnMessageReceivedDelegate OnMessageReceived;

    public static void SendMessageToJavascript(string messageJson)
    {
        try
        {
            sendMessageToJavascript(messageJson);
        }
        catch (EntryPointNotFoundException)
        {
            //Debug.Log("Could not send message as not running in web browser");
        }
    }

    [DllImport("__Internal")]
    private static extern void sendMessageToJavascript(string messageJson);

    [DllImport("__Internal")]
    private static extern void _LogToJavascript(string message);

    public static void LogToJavascript(string message)
    {
        try
        {
            _LogToJavascript(message);
        }
        catch (EntryPointNotFoundException)
        {
            //Debug.Log(message);
        }
    }
}
