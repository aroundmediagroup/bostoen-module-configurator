﻿using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.Collections.Generic;

public class MessageHandler : MonoBehaviour
{
    public static MessageHandler Instance;

    public delegate void OnViewerInitializedDelegate(string sessionId, string serverUrl);
    public static OnViewerInitializedDelegate OnViewerInitialized;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        WebPageInteractor.OnMessageReceived += ReceiveMessage;
        WebPageInteractor.SendMessageToJavascript("{\"type\":\"ViewerLoadedEvent\"}");
    }



    void ReceiveMessage(string messageJson)
    {
        var jsonEscapedString = messageJson.Replace("\\\"", "\"");

        Debug.Log(jsonEscapedString);
        Message message = SerializationHelper.DeserializeObject<Message>(jsonEscapedString);
        if (message != null)
        {
            Debug.Log("ReceiveMessage");
            if(message.type == "InitializeViewerEvent" )
            {
                InitializeMessage initializeMessage = SerializationHelper.DeserializeObject<InitializeMessage>(jsonEscapedString);
                OnViewerInitialized.Invoke(initializeMessage.sessionId, initializeMessage.serverUrl);
            }
            else
            {
                //Debug.LogError("Unity Could not read message");
                //WebPageInteractor.LogToJavascript("Unity Could not read message");
            }
        }
        else
        {
            Debug.Log("Message is null");
        }
    }

}