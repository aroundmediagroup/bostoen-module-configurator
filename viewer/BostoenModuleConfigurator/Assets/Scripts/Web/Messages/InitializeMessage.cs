﻿using System;

[Serializable]
public class InitializeMessage : Message
{
    public string sessionId;
    public string serverUrl;

    public InitializeMessage()
    {
        type = "InitializeViewerEvent";
    }
}