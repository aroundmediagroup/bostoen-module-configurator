﻿using Newtonsoft.Json;

public class SerializationHelper
{
    public static T DeserializeObject<T>(string message)
    {
        return JsonConvert.DeserializeObject<T>(message, new JsonSerializerSettings
        {
            MissingMemberHandling = MissingMemberHandling.Ignore
        });
    }

    public static string SerializeObject(object objectToSerialize)
    {
        return JsonConvert.SerializeObject(objectToSerialize, new JsonSerializerSettings
        {
            MissingMemberHandling = MissingMemberHandling.Ignore
        });
    }
}
