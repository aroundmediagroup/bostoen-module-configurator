using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using sharpPDF;
using sharpPDF.Enumerators;
using UnityEngine;

public class SaveAndDownloadPDF
{
    [DllImport("__Internal")]
    private static extern void DownloadFile(byte[] array, int byteLength, string fileName);

    public static void CreatePDF(Texture2D[] screenschots, float houseWidth, float houseLength, List<Room> modules, List<Window> windows, List<Door> doors)
    {
		int yCoord = 730;
		pdfDocument myDoc = new pdfDocument("Bostoen configuration", "Me", false);
		pdfPage myFirstPage = myDoc.addPage();

		myFirstPage.addText("My configuration", 25, yCoord, predefinedFont.csHelveticaOblique, 30, new pdfColor(predefinedColor.csBlack));
		float width = houseWidth;
		float length = houseLength;
		for (int i = 0; i < modules.Count; i++)
		{
			if (modules[i].ModuleType == ModuleType.Garage)
			{
				length += 4;
			}
		}
		yCoord -= 30;
		string totalSurface = string.Format("Width: {0}m, Length: {1}m, Total surface: {2}m\u2072", width, length, length * width);
		myFirstPage.addText(totalSurface, 25, yCoord, predefinedFont.csCourierBoldOblique, 12, new pdfColor(predefinedColor.csBlack));
		yCoord -= 25;

		//----------------------------------------Modules----------------------------------------
		/*Table's creation*/
		if (modules.Count > 0)
		{
			myFirstPage.addText("Modules", 25, yCoord, predefinedFont.csCourierBoldOblique, 12, new pdfColor(predefinedColor.csBlack));
			yCoord -= 20;
			pdfTable myTable = new pdfTable();
			//Set table's border
			myTable.borderSize = 1;
			myTable.borderColor = new pdfColor(predefinedColor.csBlack);

			/*Add Columns to a grid*/
			myTable.tableHeader.addColumn(new pdfTableColumn("Label", predefinedAlignment.csRight, 120));
			myTable.tableHeader.addColumn(new pdfTableColumn("Length", predefinedAlignment.csCenter, 120));
			myTable.tableHeader.addColumn(new pdfTableColumn("Width", predefinedAlignment.csCenter, 120));
			myTable.tableHeader.addColumn(new pdfTableColumn("Surface", predefinedAlignment.csCenter, 120));

			for (int i = 0; i < modules.Count; i++)
			{
				pdfTableRow myRow = myTable.createRow();
				myRow[0].columnValue = modules[i].GetName();
				myRow[1].columnValue = modules[i].Length + "m";
				myRow[2].columnValue = modules[i].Width+ "m";
				myRow[3].columnValue = (modules[i].Length * modules[i].Width) + "m2";
				myTable.addRow(myRow);
			}

			/*Set Header's Style*/
			myTable.tableHeaderStyle = new pdfTableRowStyle(predefinedFont.csCourierBoldOblique, 12, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.BostoenRed));
			/*Set Row's Style*/
			myTable.rowStyle = new pdfTableRowStyle(predefinedFont.csCourier, 8, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.csWhite));
			/*Set Alternate Row's Style*/
			myTable.alternateRowStyle = new pdfTableRowStyle(predefinedFont.csCourier, 8, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.csWhite));
			/*Set Cellpadding*/
			myTable.cellpadding = 10;
			/*Put the table on the page object*/
			myFirstPage.addTable(myTable, 25, yCoord);
			yCoord -= 31 * (modules.Count + 1);
			myTable = null;
		}

		//----------------------------------------Windows----------------------------------------
		if (windows.Count > 0)
		{
			yCoord -= 15;
			if(yCoord < 31 * (windows.Count + 1))
            {
				myFirstPage = myDoc.addPage();
				yCoord = 730;
			}
			myFirstPage.addText("Windows", 25, yCoord, predefinedFont.csCourierBoldOblique, 12, new pdfColor(predefinedColor.csBlack));
			yCoord -= 20;
			pdfTable myTable = new pdfTable();
			//Set table's border
			myTable.borderSize = 1;
			myTable.borderColor = new pdfColor(predefinedColor.csBlack);

			/*Add Columns to a grid*/
			myTable.tableHeader.addColumn(new pdfTableColumn("Name", predefinedAlignment.csRight, 120));
			myTable.tableHeader.addColumn(new pdfTableColumn("Width", predefinedAlignment.csCenter, 120));
			myTable.tableHeader.addColumn(new pdfTableColumn("Height", predefinedAlignment.csCenter, 120));

			for (int i = 0; i < windows.Count; i++)
			{
				pdfTableRow myRow = myTable.createRow();
				myRow[0].columnValue = windows[i].GetName();
				myRow[1].columnValue = windows[i].WindowWidthInCm + "cm";
				myRow[2].columnValue = windows[i].WindowHeightInCm + "cm";
				myTable.addRow(myRow);
			}

			/*Set Header's Style*/
			myTable.tableHeaderStyle = new pdfTableRowStyle(predefinedFont.csCourierBoldOblique, 12, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.BostoenRed));
			/*Set Row's Style*/
			myTable.rowStyle = new pdfTableRowStyle(predefinedFont.csCourier, 8, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.csWhite));
			/*Set Alternate Row's Style*/
			myTable.alternateRowStyle = new pdfTableRowStyle(predefinedFont.csCourier, 8, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.csWhite));
			/*Set Cellpadding*/
			myTable.cellpadding = 10;
			/*Put the table on the page object*/
			myFirstPage.addTable(myTable, 25, yCoord);
			yCoord -= 31 * (windows.Count + 1);
			myTable = null;
		}

		//----------------------------------------Doors----------------------------------------
		if (doors.Count > 0)
		{
			yCoord -= 15;
			if (yCoord < 31 * (doors.Count + 1))
			{
				myFirstPage = myDoc.addPage();
				yCoord = 730;
			}
			myFirstPage.addText("Doors", 25, yCoord, predefinedFont.csCourierBoldOblique, 12, new pdfColor(predefinedColor.csBlack));
			yCoord -= 20;
			pdfTable myTable = new pdfTable();
			//Set table's border
			myTable.borderSize = 1;
			myTable.borderColor = new pdfColor(predefinedColor.csBlack);

			/*Add Columns to a grid*/
			myTable.tableHeader.addColumn(new pdfTableColumn("Name", predefinedAlignment.csRight, 120));
			myTable.tableHeader.addColumn(new pdfTableColumn("Width", predefinedAlignment.csCenter, 120));
			myTable.tableHeader.addColumn(new pdfTableColumn("Height", predefinedAlignment.csCenter, 120));

			for (int i = 0; i < doors.Count; i++)
			{
				pdfTableRow myRow = myTable.createRow();
				myRow[0].columnValue = doors[i].GetName();
				myRow[1].columnValue = doors[i].DoorWidthInCm + "cm";
				myRow[2].columnValue = doors[i].DoorHeightInCm + "cm";
				myTable.addRow(myRow);
			}

			/*Set Header's Style*/
			myTable.tableHeaderStyle = new pdfTableRowStyle(predefinedFont.csCourierBoldOblique, 12, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.BostoenRed));
			/*Set Row's Style*/
			myTable.rowStyle = new pdfTableRowStyle(predefinedFont.csCourier, 8, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.csWhite));
			/*Set Alternate Row's Style*/
			myTable.alternateRowStyle = new pdfTableRowStyle(predefinedFont.csCourier, 8, new pdfColor(predefinedColor.csBlack), new pdfColor(predefinedColor.csWhite));
			/*Set Cellpadding*/
			myTable.cellpadding = 10;
			/*Put the table on the page object*/
			myFirstPage.addTable(myTable, 25, yCoord);
			yCoord -= 31 * (doors.Count + 1);
			myTable = null;
		}

		//----------------------------------------Screenschots----------------------------------------
		pdfPage page = myDoc.addPage(1584, 1224);
		yCoord = 1584;
		for (int i = 0; i < screenschots.Length; i++)
		{
			yCoord -= screenschots[i].height + 20;
			page.addImage(screenschots[i].EncodeToJPG(), 35, yCoord, screenschots[i].height, screenschots[i].width);
		}

		myDoc.createPDF("BostoenConfiguration.pdf");
		byte[] data = File.ReadAllBytes("BostoenConfiguration.pdf");
		DownloadFile(data, data.Length, "BostoenConfiguration.pdf");
	}
}
