using System;
using System.Text;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine;

public class SessionService {
    public static IEnumerator CreateSession(Session session, Action<SessionResponse> onComplete)
    {
        CreateSessionRequest createSessionRequest = new CreateSessionRequest(session);
        string bodyJsonString = SerializationHelper.SerializeObject(createSessionRequest);

        string targetUrl = "/api/create-session/";
        Debug.Log("targetUrl: " + targetUrl);

        using (UnityWebRequest request = new UnityWebRequest(targetUrl, "POST"))
        {
            byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
            request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");

            yield return request.SendWebRequest();
            while (!request.isDone) yield return null;
            LogRequest(request);

            byte[] result = request.downloadHandler.data;
            string sessionJSON = System.Text.Encoding.Default.GetString(result);
            Debug.Log(result);
            Debug.Log(sessionJSON);

            SessionResponse createSessionResponse = JsonUtility.FromJson<SessionResponse>(sessionJSON);
            onComplete(createSessionResponse);
        }
    }

    public static IEnumerator UpdateSession(string sessionId, Session session, Action<SessionResponse> onComplete)
    {
        UpdateSessionRequest updateSessionRequest = new UpdateSessionRequest(sessionId, session);
        string bodyJsonString = SerializationHelper.SerializeObject(updateSessionRequest);

        string targetUrl = "/api/update-session/";
        Debug.Log("targetUrl: " + targetUrl);

        using (UnityWebRequest request = new UnityWebRequest(targetUrl, "POST"))
        {
            byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
            request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");

            yield return request.SendWebRequest();
            while (!request.isDone) yield return null;
            LogRequest(request);

            byte[] result = request.downloadHandler.data;
            string sessionJSON = System.Text.Encoding.Default.GetString(result);
            Debug.Log(result);
            Debug.Log(sessionJSON);

            SessionResponse updateSessionResponse = JsonUtility.FromJson<SessionResponse>(sessionJSON);
            onComplete(updateSessionResponse);
        }
    }

    public static IEnumerator GetSession(string sessionId, Action<GetSessionResponse> onComplete)
    {
        string targetUrl = String.Format("/api/get-session/{0}", sessionId);
        Debug.Log("targetUrl: " + targetUrl);

        using (UnityWebRequest request = UnityWebRequest.Get(targetUrl))
        {
            yield return request.SendWebRequest();
            while (!request.isDone) yield return null;
            LogRequest(request);

            byte[] result = request.downloadHandler.data;
            string sessionJSON = System.Text.Encoding.Default.GetString(result);
            Debug.Log(result);
            Debug.Log(sessionJSON);

            GetSessionResponse getSessionResponse = SerializationHelper.DeserializeObject<GetSessionResponse>(sessionJSON);
            onComplete(getSessionResponse);
        }
    }

    private static void LogRequest(UnityWebRequest request)
    {
        Debug.Log($"isConnectionError: {request.result == UnityWebRequest.Result.ConnectionError}");
        Debug.Log($"isProtocolError: {request.result == UnityWebRequest.Result.ProtocolError}");
        Debug.Log($"responseCode: {request.responseCode}");
        Debug.Log($"error: {request.error}");
        Debug.Log($"Response:  {request.downloadHandler?.text}");
    }
}

[Serializable]
public class CreateSessionRequest
{
    public CreateSessionRequest(Session session)
    {
        this.session = session;
    }
    public Session session;
}

[Serializable]
public class UpdateSessionRequest
{
    public UpdateSessionRequest(string sessionId, Session session)
    {
        this.id = sessionId;
        this.session = session;
    }
    public string id;
    public Session session;
}

[Serializable]
public class GetSessionResponse
{
    public string status;
    public Session session;
    public string error;
    public bool isFound;

    public bool IsSuccess() { return status == "success"; }
}

[Serializable]
public class SessionResponse
{
    public string status;
    public string error;
    public string id;

    public bool IsSuccess() { return status == "success"; }
}
