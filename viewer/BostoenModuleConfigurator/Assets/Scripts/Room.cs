using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ModuleType
{
    Kitchen = 0,
    Bedroom1 = 1,
    Bedroom2 = 2,
    Bedroom3 = 3,
    Bathroom = 4,
    BathroomShower = 5,
    Garage = 6,
    Hall = 7,
    Living = 8,
    Diningroom = 9,
    Storage = 10,
    Toilet = 11,

    Undefined = -1,
}

public class Room : MonoBehaviour
{
    [SerializeField] private Wall LeftWall;
    [SerializeField] private Wall RightWall;
    [SerializeField] private Wall FrontWall;
    [SerializeField] private Wall BackWall;
    [SerializeField] private Transform _highlight;

    [SerializeField] private BoxCollider _collider;

    [SerializeField] private float _defaultWidth = 7;
    [SerializeField] private float _defaultLength = 9;
    [SerializeField] private float _minWidth = 7;
    [SerializeField] private float _minLength = 9;
    [SerializeField] private float _wallOffset = 0.15f;

    [SerializeField] private ModuleType _moduleType = ModuleType.Undefined;
    [SerializeField] private string _defaultName = "Module";
    [SerializeField] private Sprite _overviewImage;

    private string _name = null;
    private bool _isInitialized = false;
    private bool _isPlaced = false;

    private int _roomCount = 0;
    private float _colliderBuffer = 0.1f;

    public float Width
    {
        get;
        private set;
    }
    public float Length
    {
        get;
        private set;
    }
    public ModuleType ModuleType
    {
        get
        {
            return _moduleType;
        }
    }
    public Sprite Image
    {
        get
        {
            return _overviewImage;
        }
    }
    public bool IsValidPosition
    {
        get;
        private set;
    } = true;
    public bool IsPlaced
    {
        get
        {
            return _isPlaced;
        }
    }

    static int lastId = 0;
    public int Id
    {
        get
        {
            return _id;
        }
        set
        {
            lastId = value;
            _id = lastId++;
            _isIdOverridden = true;
        }
    }
    private int _id;
    bool _isIdOverridden = false;

    private void Start()
    {
        if (_isIdOverridden == false)
        {
            _id = lastId++;
        }
        Initialize();
    }

    private void Update()
    {
        if (_isPlaced == false && ModuleType != ModuleType.Garage)
        {
            if (IsValidPosition == false)
            {
                if (Width > _minWidth)
                {
                    SetWidth(Width - 1);
                }
                if (Length > _minLength)
                {
                    SetLength(Length - 1);
                }
            }
            else
            {
                LayerMask mask = LayerMask.GetMask("Room");
                Collider[] overlappingRooms = Physics.OverlapBox(transform.position, new Vector3(Mathf.Clamp((Length + 1) / 2f, _minLength / 2f, _defaultLength / 2f), 0.5f, Mathf.Clamp((Width + 1)/2f, _minWidth / 2f, _defaultWidth / 2f)), Quaternion.identity, mask);
                if (overlappingRooms.Length <= 1)
                {
                    if (_isPlaced == false)
                    {
                        if (Width < _defaultWidth)
                        {
                            SetWidth(Width + 1);
                        }
                        if (Length < _defaultLength)
                        {
                            SetLength(Length + 1);
                        }
                    }
                }
            }
        }
    }

    public void Initialize()
    {
        if (_isInitialized == false)
        {
            _name = string.Format("{0}_{1}", _defaultName, Id);

            Length = _defaultLength;
            Width = _defaultWidth;
            LeftWall.ParentRoom = this;
            LeftWall.SetLength(Length);
            RightWall.ParentRoom = this;
            RightWall.SetLength(Length);
            FrontWall.ParentRoom = this;
            FrontWall.SetLength(Width);
            BackWall.ParentRoom = this;
            BackWall.SetLength(Width);
            SetWallPositions();
            SetHighlightActive(false);
            _isInitialized = true;
        }
    }

    public void SetWidth(float width)
    {
        Width = width;
        FrontWall.SetLength(Width);
        BackWall.SetLength(Width);
        SetWallPositions();
    }

    public void SetLength(float length)
    {
        Length = length;
        LeftWall.SetLength(Length);
        RightWall.SetLength(Length);
        SetWallPositions();
    }

    public void SetPlaced(bool isPlaced)
    {
        _isPlaced = isPlaced;
    }

    public void SetWallActive(WallType type, bool isActive)
    {
        switch (type)
        {
            case WallType.Front:
                {
                    FrontWall.gameObject.SetActive(isActive);
                }
                break;
            case WallType.Back:
                {
                    BackWall.gameObject.SetActive(isActive);
                }
                break;
            case WallType.Left:
                {
                    LeftWall.gameObject.SetActive(isActive);
                }
                break;
            case WallType.Right:
                {
                    RightWall.gameObject.SetActive(isActive);
                }
                break;
        }
    }

    public Wall GetWall(WallType type)
    {
        switch (type)
        {
            case WallType.Front:
                {
                    return FrontWall;
                }
            case WallType.Back:
                {
                    return BackWall;
                }
            case WallType.Left:
                {
                    return LeftWall;
                }
            case WallType.Right:
                {
                    return RightWall;
                }
            default:
                {
                    return FrontWall;
                }
        }
    }

    private void SetWallPositions()
    {
        FrontWall.SetPosition(new Vector3((Length - _wallOffset) / 2f, 0, Width / 2f));
        BackWall.SetPosition(new Vector3(-(Length - _wallOffset) / 2f, 0, -Width / 2f));
        LeftWall.SetPosition(new Vector3(Length / 2f, 0, -(Width - _wallOffset) / 2f));
        RightWall.SetPosition(new Vector3(-Length / 2f, 0, (Width - _wallOffset) / 2f));

        Vector3 size = _collider.size;
        size.x = Length - _colliderBuffer;
        size.z = Width - _colliderBuffer;
        _collider.size = size;

        _highlight.localScale = new Vector3(Length, Width, 1);
    }

    public string GetName()
    {
        return _name;
    }

    public string GetDefaultName()
    {
        return _defaultName;
    }

    public void SetName(string name)
    {
        _name = name;
    }

    public void SetHighlightActive(bool isActive)
    {
        _highlight.gameObject.SetActive(isActive);
    }

    public void ClearOverlap()
    {
        _roomCount = 0;
        IsValidPosition = true;
        SetHighlightActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            _roomCount++;
            IsValidPosition = false;
            SetHighlightActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            _roomCount--;
            if (_roomCount == 0)
            {
                IsValidPosition = true;
                SetHighlightActive(false);
            }
        }
    }
}
