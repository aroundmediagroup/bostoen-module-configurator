using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    [SerializeField] private HouseFloor _groundFloor;
    [SerializeField] private HouseFloor _firstFloor;

    [SerializeField] private MeasureLine _widthMeasureLine;
    [SerializeField] private MeasureLine _lengthMeasureLine;
    [SerializeField] private MeasureLine _heightMeasureLine;
    [SerializeField] private BoxCollider _garageLeft;
    [SerializeField] private BoxCollider _garageRight;
    [SerializeField] private float _garageWidth = 4;

    public float Width
    {
        get
        {
            return _groundFloor.Width;
        }
    }
    public float Length
    {
        get
        {
            return _groundFloor.Length;
        }
    }

    public void SetWidth(float width)
    {
        _widthMeasureLine.SetSizeInM(width);
        _groundFloor.SetWidth(width);
        _firstFloor.SetWidth(width);
        SetMeusureLineTransforms();
    }

    public void SetLength(float length)
    {
        _lengthMeasureLine.SetSizeInM(length);
        _groundFloor.SetLength(length);
        _firstFloor.SetLength(length);
        SetMeusureLineTransforms();
    }

    public void SetFirstFloorActive(bool isActive)
    {
        _firstFloor.gameObject.SetActive(isActive);
    }

    public void SetMeasureLinesActive(bool isActive)
    {
        _widthMeasureLine.SetActive(isActive);
        _lengthMeasureLine.SetActive(isActive);
        _heightMeasureLine.SetActive(isActive);
    }

    public HouseFloor GetFloor(int level)
    {
        switch(level)
        {
            case 0:
                {
                    return _groundFloor;
                }
            case 1:
                {
                    return _firstFloor;
                }
        }
        return _groundFloor;
    }

    public Transform GetGaragePlaceholder(bool isLeft)
    {
        if(isLeft)
        {
            return _garageLeft.transform;
        }
        else
        {
            return _garageRight.transform;
        }
    }

    private void SetMeusureLineTransforms()
    {
        Vector3 pos = _widthMeasureLine.transform.position;
        pos.x = (Length / 2f) + 6;
        _widthMeasureLine.transform.position = pos; 
        
        pos = _lengthMeasureLine.transform.position;
        pos.z = -((Width / 2f) + 2);
        _lengthMeasureLine.transform.position = pos;
        
        pos = _heightMeasureLine.transform.position;
        pos.x = ((Length / 2f) + 6);
        pos.z = ((Width / 2f));
        _heightMeasureLine.transform.position = pos;

        _garageLeft.size = new Vector3(_garageWidth, 0.1f, Width);
        _garageLeft.transform.position = new Vector3((Length / 2f) + (_garageWidth/2f), 0, 0);
        _garageRight.size = new Vector3(_garageWidth, 0.1f, Width);
        _garageRight.transform.position = new Vector3(-(Length / 2f) - (_garageWidth / 2f), 0, 0);
    }
}
