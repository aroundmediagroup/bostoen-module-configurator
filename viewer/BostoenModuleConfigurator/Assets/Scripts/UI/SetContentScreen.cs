using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SetContentScreen : MonoBehaviour
{
    public event ConfirmButtonClickedHandler OnConfirmClicked;
    public event ConfirmButtonClickedHandler OnBackClicked;

    public event SelectContentHandler OnModuleSelected;
    public event SelectContentHandler OnWindowSelected;
    public event SelectContentHandler OnOutsideDoorSelected;
    public event SelectContentHandler OnInsideDoorSelected;

    [SerializeField] private Toggle _modulesToggle;
    [SerializeField] private Toggle _windowsToggle;
    [SerializeField] private Toggle _outsideDoorsToggle;
    [SerializeField] private Toggle _insideDoorsToggle;

    [SerializeField] private GameObject _modulesTab;
    [SerializeField] private GameObject _windowsTab;
    [SerializeField] private GameObject _outsideDoorsTab;
    [SerializeField] private GameObject _insideDoorsTab;

    [SerializeField] private Transform _moduleButtonsParent;
    [SerializeField] private Transform _windowButtonsParent;
    [SerializeField] private Transform _outsideDoorButtonsParent;
    [SerializeField] private Transform _insideDoorButtonsParent;
    [SerializeField] private DragAndDropButton _buttonPrefab;

    [SerializeField] private Button _confirmButton;
    [SerializeField] private Button _backButton;

    void Start()
    {
        _confirmButton.onClick.AddListener(ConfirmClicked);
        _backButton.onClick.AddListener(BackClicked);

        _modulesToggle.onValueChanged.AddListener(OnModulesValueChanged);
        _windowsToggle.onValueChanged.AddListener(OnWindowsValueChanged);
        _outsideDoorsToggle.onValueChanged.AddListener(OnDoorsValueChanged);
        _insideDoorsToggle.onValueChanged.AddListener(OnInsideDoorsValueChanged);
    }

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    public void SetModules(Room[] modules)
    {
        for (int i = 0; i < modules.Length; i++)
        {
            DragAndDropButton button = Instantiate(_buttonPrefab, _moduleButtonsParent);
            button.SetImage(modules[i].Image);
            button.SetName(modules[i].GetDefaultName());
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            int index = i;
            entry.callback.AddListener((data) => { ModuleSelected(index); });
            button.EventTrigger.triggers.Add(entry);
        }
    }

    public void SetWindows(Window[] windows)
    {
        for(int i = 0; i < windows.Length; i++)
        {
            DragAndDropButton button = Instantiate(_buttonPrefab, _windowButtonsParent);
            button.SetImage(windows[i].Image);
            button.SetName(windows[i].GetName());
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            int index = i;
            entry.callback.AddListener((data) => { WindowSelected(index); });
            button.EventTrigger.triggers.Add(entry);
        }
    }

    public void SetOutsideDoors(Door[] doors)
    {
        for (int i = 0; i < doors.Length; i++)
        {
            DragAndDropButton button = Instantiate(_buttonPrefab, _outsideDoorButtonsParent);
            button.SetImage(doors[i].Image);
            button.SetName(doors[i].GetName());
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            int index = i;
            entry.callback.AddListener((data) => { DoorSelected(index); });
            button.EventTrigger.triggers.Add(entry);
        }
    }

    public void SetInsideDoors(Door[] doors)
    {
        for (int i = 0; i < doors.Length; i++)
        {
            DragAndDropButton button = Instantiate(_buttonPrefab, _insideDoorButtonsParent);
            button.SetImage(doors[i].Image);
            button.SetName(doors[i].GetName());
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            int index = i;
            entry.callback.AddListener((data) => { InsideDoorSelected(index); });
            button.EventTrigger.triggers.Add(entry);
        }
    }

    private void ConfirmClicked()
    {
        OnConfirmClicked?.Invoke(ScreenType.SetContents);
    }

    private void BackClicked()
    {
        OnBackClicked?.Invoke(ScreenType.SetContents);
    }

    private void OnModulesValueChanged(bool isActive)
    {
        _modulesTab.SetActive(isActive);
    }

    private void OnWindowsValueChanged(bool isActive)
    {
        _windowsTab.SetActive(isActive);
    }

    private void OnDoorsValueChanged(bool isActive)
    {
        _outsideDoorsTab.SetActive(isActive);
    }

    private void OnInsideDoorsValueChanged(bool isActive)
    {
        _insideDoorsTab.SetActive(isActive);
    }

    public void ModuleSelected(int id)
    {
        OnModuleSelected?.Invoke(id);
    }

    public void WindowSelected(int id)
    {
        OnWindowSelected?.Invoke(id);
    }

    public void DoorSelected(int id)
    {
        OnOutsideDoorSelected?.Invoke(id);
    }

    public void InsideDoorSelected(int id)
    {
        OnInsideDoorSelected?.Invoke(id);
    }

}
