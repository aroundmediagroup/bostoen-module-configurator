using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetLabelsScreen : MonoBehaviour
{
    public event ConfirmButtonClickedHandler OnConfirmClicked;
    public event ConfirmButtonClickedHandler OnBackClicked;

    [SerializeField] private Button _confirmButton;
    [SerializeField] private Button _backButton;

    [SerializeField] private Transform _inputfieldsParent;
    [SerializeField] private InputField _inputFieldPrefab;

    private List<InputField> _inputFields = new List<InputField>();

    void Start()
    {
        _confirmButton.onClick.AddListener(ConfirmClicked);
        _backButton.onClick.AddListener(BackClicked);
    }

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    private void ConfirmClicked()
    {
        OnConfirmClicked?.Invoke(ScreenType.SetLabels);
    }

    private void BackClicked()
    {
        OnBackClicked?.Invoke(ScreenType.SetLabels);
    }

    public InputField GetInputField(string placeholder, Transform target)
    {
        InputField inputField = Instantiate(_inputFieldPrefab, _inputfieldsParent);
        inputField.placeholder.GetComponent<Text>().text = placeholder;
        inputField.GetComponent<UIWithWorldTarget>().SetTarget(target);
        _inputFields.Add(inputField);
        return inputField;
    }

    public void ClearInputFields()
    {
        for(int i = 0; i < _inputFields.Count; i++)
        {
            _inputFields[i].onValueChanged.RemoveAllListeners();
            Destroy(_inputFields[i].gameObject);
        }
        _inputFields.Clear();
    }
}
