using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum OverviewItemType
{
    Module,
    Window,
    Door
}

public class OverviewItem : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private TMP_Text _nameText;
    [SerializeField] private TMP_Text _roomText;
    [SerializeField] private TMP_Text _dimensionsText;

    public void SetImage(Sprite image)
    {
        _image.sprite = image;
    }

    public void SetName(string name)
    {
        _nameText.text = name;
    }

    public void SetParentRoomName(string name)
    {
        _roomText.gameObject.SetActive(string.IsNullOrEmpty(name) == false);
        _roomText.text = name;
    }

    public void SetDimensions(float width, float length)
    {
        _dimensionsText.text = string.Format("{0}cm x {1}cm \n{2}m<sup>2<sup>", width, length, (width / 100f) * (length/100f)) ;
    }

    public void SetType(OverviewItemType type)
    {
        _roomText.gameObject.SetActive(type == OverviewItemType.Door);
        //_dimensionsText.gameObject.SetActive(type == OverviewItemType.Module);
    }
}
