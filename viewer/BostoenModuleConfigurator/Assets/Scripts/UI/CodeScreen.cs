using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CodeScreen : MonoBehaviour
{
    public event ButtonClickedHandler OnCloseClicked;
    public event ButtonClickedHandler OnConfirmClicked;

    [SerializeField] private Button _closeButton;
    [SerializeField] private Button _confirmButton;
    [SerializeField] private TMP_InputField _codeInput;

    void Start()
    {
        _closeButton.onClick.AddListener(CloseClicked);
        _confirmButton.onClick.AddListener(ConfirmClicked);
    }

    private void CloseClicked()
    {
        OnCloseClicked?.Invoke();
    }


    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    public void SetCOnfirmButtonActive(bool isActive)
    {
        _confirmButton.gameObject.SetActive(isActive);
    }

    public void SetCode(string code)
    {
        _codeInput.text = code;
    }

    public string GetCode()
    {
        return _codeInput.text;
    }

    private void ConfirmClicked()
    {
        OnConfirmClicked?.Invoke();
    }
}
