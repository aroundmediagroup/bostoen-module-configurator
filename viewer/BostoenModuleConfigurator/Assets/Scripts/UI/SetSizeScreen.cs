using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SetSizeScreen : MonoBehaviour
{
    public event ConfirmButtonClickedHandler OnConfirmClicked;
    public event ButtonClickedHandler OnCodeButtonClicked;
    public event SliderValueChangedHandler OnWidthSliderValueChanged;
    public event SliderValueChangedHandler OnLengthSliderValueChanged;
    public event SelectContentHandler OnExtendFirstFloorValueChanged;

    [SerializeField] private Slider _widthSlider;
    [SerializeField] private Slider _lengthSlider;
    [SerializeField] private Button _confirmButton;
    [SerializeField] private Button _codeButton;
    [SerializeField] private TMP_Dropdown _extendFirstFloorDropdown;

    void Start()
    {
        _widthSlider.onValueChanged.AddListener(WidthSliderValueChanged);
        _lengthSlider.onValueChanged.AddListener(LengthSliderValueChanged);
        _confirmButton.onClick.AddListener(ConfirmClicked);
        _codeButton.onClick.AddListener(CodeButtonClicked);

        WidthSliderValueChanged(_widthSlider.value);
        LengthSliderValueChanged(_lengthSlider.value);

        _extendFirstFloorDropdown.onValueChanged.AddListener(ExtendFirstFloorValueChanged);
    }

    private void ExtendFirstFloorValueChanged(int value)
    {
        OnExtendFirstFloorValueChanged?.Invoke(value);
    }

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    private void WidthSliderValueChanged(float newValue)
    {
        OnWidthSliderValueChanged?.Invoke(newValue);
    }

    private void LengthSliderValueChanged(float newValue)
    {
        OnLengthSliderValueChanged?.Invoke(newValue);
    }

    private void ConfirmClicked()
    {
        OnConfirmClicked?.Invoke(ScreenType.SetSize);
    }

    private void CodeButtonClicked()
    {
        OnCodeButtonClicked?.Invoke();
    }
}
