using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionPanel : MonoBehaviour
{
    public event ButtonClickedHandler OnDeleteButtonClicked;
    public event ButtonClickedHandler OnRotateButtonClicked;
    public event ButtonClickedHandler OnMoveButtonDown;

    [SerializeField] private UIWithWorldTarget _uiTarget;
    [SerializeField] private Button _deleteButton;
    [SerializeField] private Button _rotateButton;

    private void Start()
    {
        _deleteButton.onClick.AddListener(DeleteButtonClicked);
        _rotateButton.onClick.AddListener(RotateButtonClicked);
    }

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    public void SetRotateActive(bool isActive)
    {
        _rotateButton.gameObject.SetActive(isActive);
    }

    public void SetUITarget(Transform target)
    {
        _uiTarget.SetTarget(target);
    }

    public void MoveButtonDown()
    {
        OnMoveButtonDown?.Invoke();
    }

    public bool GetIsActive()
    {
        return gameObject.activeSelf;
    }

    private void DeleteButtonClicked()
    {
        OnDeleteButtonClicked?.Invoke();
    }

    private void RotateButtonClicked()
    {
        OnRotateButtonClicked?.Invoke();
    }
}
