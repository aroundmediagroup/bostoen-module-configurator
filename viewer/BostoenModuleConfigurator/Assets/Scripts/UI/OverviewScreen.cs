using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OverviewScreen : MonoBehaviour
{
    public event ButtonClickedHandler OnGetCodeClicked;
    public event ConfirmButtonClickedHandler OnBackClicked;
    public event ButtonClickedHandler OnDownloadClicked;

    [SerializeField] private Button _confirmButton;
    [SerializeField] private Button _backButton;
    [SerializeField] private Button _downloadButton;

    [SerializeField] private TMP_Text _surfaceText;
    [SerializeField] private Transform _overviewItemParent;
    [SerializeField] private OverviewItem _overviewItemPrefab;

    private List<OverviewItem> _items = new List<OverviewItem>();

    void Start()
    {
        _confirmButton.onClick.AddListener(ConfirmClicked);
        _backButton.onClick.AddListener(BackClicked);
        _downloadButton.onClick.AddListener(DownloadClicked);
    }

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    private void ConfirmClicked()
    {
        OnGetCodeClicked?.Invoke();
    }

    private void DownloadClicked()
    {
        OnDownloadClicked?.Invoke();
    }

    private void BackClicked()
    {
        OnBackClicked?.Invoke(ScreenType.Overview);
    }

    public void SetTotalSurfaceText(float totalSurface)
    {
        _surfaceText.text = string.Format("Total surface: {0}m<sup>2<sup>", totalSurface);
    }

    public OverviewItem AddItem(OverviewItemType type, string name)
    {
        OverviewItem item = Instantiate(_overviewItemPrefab, _overviewItemParent);
        item.SetType(type);
        item.SetName(name);
        _items.Add(item);
        return item;
    }

    public void ClearItems()
    {
        for (int i = 0; i < _items.Count; i++)
        {
            Destroy(_items[i].gameObject);
        }
        _items.Clear();
    }
}
