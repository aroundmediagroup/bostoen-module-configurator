using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class UIWithWorldTarget : MonoBehaviour
{
    private Transform _target;
    private RectTransform _rectTransform;
    private Camera _mainCam = null;

    private void Start()
    {
        _mainCam = Camera.main;
        _rectTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        if(_target)
        {
            _rectTransform.anchoredPosition = _mainCam.WorldToScreenPoint(_target.position);
        }
    }

    public void SetTarget(Transform target)
    {
        _target = target;
    }
}
