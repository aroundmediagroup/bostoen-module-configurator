using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialScreen : MonoBehaviour
{
    public event ButtonClickedHandler OnCloseClicked;

    [SerializeField] private Button _closeButton;

    void Start()
    {
        _closeButton.onClick.AddListener(CloseClicked);
    }

    private void CloseClicked()
    {
        OnCloseClicked?.Invoke();
    }


    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }
}
