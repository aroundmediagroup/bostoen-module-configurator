using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersistenUI : MonoBehaviour
{
    public event ButtonClickedHandler OnResetClicked;
    public event ButtonClickedHandler OnRecenterClicked;
    public event ButtonClickedHandler OnMesurementsClicked;
    public event ButtonClickedHandler OnTutorialClicked;
    public event ButtonClickedHandler OnContactClicked;

    [SerializeField] private Button _resetButton;
    [SerializeField] private Button _recenterButton;
    [SerializeField] private Button _mesurementsButton;
    [SerializeField] private Button _tutorialButton;
    [SerializeField] private Button _contactButton;

    void Start()
    {
        _resetButton.onClick.AddListener(ResetClicked);
        _recenterButton.onClick.AddListener(RecenterClicked);
        _mesurementsButton.onClick.AddListener(MesurementsClicked);
        _tutorialButton.onClick.AddListener(TutorialClicked);
        _contactButton.onClick.AddListener(ContactClicked);
    }

    private void ResetClicked()
    {
        OnResetClicked?.Invoke();
    }

    private void RecenterClicked()
    {
        OnRecenterClicked?.Invoke();
    }

    private void MesurementsClicked()
    {
        OnMesurementsClicked?.Invoke();
    }

    private void TutorialClicked()
    {
        OnTutorialClicked?.Invoke();
    }

    private void ContactClicked()
    {
        OnContactClicked?.Invoke();
    }
}
