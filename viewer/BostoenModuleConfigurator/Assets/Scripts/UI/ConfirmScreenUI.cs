using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum ConfirmType
{
    ClearHouse,

    Undefined = -1
}

public class ConfirmScreenUI : MonoBehaviour
{
    public event ButtonClickedHandler OnConfirmClicked;
    public event ButtonClickedHandler OnCancelClicked;

    [HideInInspector] public ConfirmType ConfirmType = ConfirmType.Undefined;

    [SerializeField] private Button _confirmButton;
    [SerializeField] private Button _cancelButton;

    [SerializeField] private TMP_Text _text;

    void Start()
    {
        _confirmButton.onClick.AddListener(ConfirmClicked);
        _cancelButton.onClick.AddListener(CancelClicked);
    }

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    private void ConfirmClicked()
    {
        OnConfirmClicked?.Invoke();
    }

    private void CancelClicked()
    {
        OnCancelClicked?.Invoke();
    }

    public void SetText(string text)
    {
        _text.text = text;
    }
}
