using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectFloorScreen : MonoBehaviour
{
    public event ToggleValueChangedHandler OnFirstFloorToggleChanged;

    [SerializeField] private Toggle _firstFloorToggle;

    private void Start()
    {
        _firstFloorToggle.onValueChanged.AddListener(FirstFloorToggleChanged);
        FirstFloorToggleChanged(_firstFloorToggle.isOn);
    }

    private void FirstFloorToggleChanged(bool newValue)
    {
        OnFirstFloorToggleChanged?.Invoke(newValue);
    }
}
