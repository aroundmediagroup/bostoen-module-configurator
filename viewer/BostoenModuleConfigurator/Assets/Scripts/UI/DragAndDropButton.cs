using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragAndDropButton : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private EventTrigger _eventTrigger;
    [SerializeField] private TMP_Text _name;

    public EventTrigger EventTrigger
    {
        get
        {
            return _eventTrigger;
        }
    }

    public void SetImage(Sprite sprite)
    {
        _image.sprite = sprite;
    }

    public void SetName(string name)
    {
        _name.text = name;
    }
}
