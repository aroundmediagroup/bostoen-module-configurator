using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseFloor : MonoBehaviour
{
    [SerializeField] private Wall LeftWall;
    [SerializeField] private Wall RightWall;
    [SerializeField] private Wall FrontWall;
    [SerializeField] private Wall BackWall;
    [SerializeField] private FloorPiece _floorPiecePrefab;
    [SerializeField] private float _wallOffset = 0.25f;

    private List<FloorPiece> _floorPiecePool = new List<FloorPiece>();

    private List<FloorPiece> _currenActiveFloorPieces = new List<FloorPiece>();

    public float Width
    {
        get
        {
            return _width;
        }
    }
    public float Length
    {
        get
        {
            return _length;
        }
    }

    private float _width = 7;
    private float _length = 9;

    public void SetWidth(float width)
    {
        _width = width;
        FrontWall.SetLength(Width);
        BackWall.SetLength(Width);
        UpdateTransforms();
    }

    public void SetLength(float length)
    {
        _length = length;
        LeftWall.SetLength(Length);
        RightWall.SetLength(Length);
        UpdateTransforms();
    }

    public Wall GetWall(WallType type)
    {
        switch (type)
        {
            case WallType.Front:
                {
                    return FrontWall;
                }
            case WallType.Back:
                {
                    return BackWall;
                }
            case WallType.Left:
                {
                    return LeftWall;
                }
            case WallType.Right:
                {
                    return RightWall;
                }
            default:
                {
                    return FrontWall;
                }
        }
    }

    private void UpdateTransforms()
    {
        FrontWall.SetPosition(new Vector3((Length - _wallOffset) / 2f, 0, Width / 2f));
        BackWall.SetPosition(new Vector3(-(Length - _wallOffset) / 2f, 0, -Width / 2f));
        LeftWall.SetPosition(new Vector3(Length / 2f, 0, -(Width - _wallOffset) / 2f));
        RightWall.SetPosition(new Vector3(-Length / 2f, 0, (Width - _wallOffset) / 2f));

        SetAllPiecesActive(false);
        _currenActiveFloorPieces.Clear();
        int panelCountLength = (int)Length;
        int panelCountWidth = (int)Width;
        for (int x = 0; x < panelCountLength * 2; x++)
        {
            for (int y = 0; y < panelCountWidth * 2; y++)
            {
                FloorPiece floorPiece = GetFloorPiece();
                floorPiece.ParentFloor = this;
                floorPiece.transform.localPosition = new Vector3((panelCountLength / 2f) - (x * 0.5f) - 0.25f, 0, (panelCountWidth / 2f) - (y * 0.5f) - 0.25f);
                _currenActiveFloorPieces.Add(floorPiece);
            }
        }
    }

    private void SetAllPiecesActive(bool isActive)
    {
        for (int i = 0; i < _currenActiveFloorPieces.Count; i++)
        {
            _currenActiveFloorPieces[i].SetActive(isActive);
        }
    }

    private FloorPiece GetFloorPiece()
    {
        FloorPiece floor = _floorPiecePool.Find(piece => piece.gameObject.activeSelf == false);
        if (floor == null)
        {
            floor = Instantiate(_floorPiecePrefab, transform);
            _floorPiecePool.Add(floor);
        }
        else
        {
            floor.SetActive(true);
        }
        return floor;
    }
}
