using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WindowType
{
    Window1 = 0,
    Window1H = 1,
    Window2 = 2,
    Window2Bis = 3,
    Window3 = 4,
    Window3Bis = 5,
    Window4 = 6,
    Window4Bis = 7,
    Window4C = 8,
    Window5 = 9,
    Window5Bis = 10,
    Window6 = 11,
    Window6Bis = 12,
    Window7A = 13,
    Window7B = 14,
    Window7C = 15,
    Window7D = 16,
    Window8 = 17,
    Window9 = 18,
    Window10 = 19,
    Window11 = 20,
    Window12 = 21,
    Window13 = 22,
    Window14 = 22,

    Undefined = -1,
}

public class Window : MonoBehaviour
{
    [HideInInspector]  public WallPiece ParentWallPiece;
    [SerializeField] private WindowType _windowType = WindowType.Undefined;
    [SerializeField] private Sprite _overviewImage;
    [SerializeField] private string _overviewName = "Window";
    [SerializeField] private int _width = 1;
    public int WindowWidthInCm = 50;
    public int WindowHeightInCm = 50;

    public WindowType WindowType
    {
        get
        {
            return _windowType;
        }
    }
    public Sprite Image
    {
        get
        {
            return _overviewImage;
        }
    }
    public int Width
    {
        get
        {
            return _width;
        }
    }

    static int lastId = 0;
    public int Id
    {
        get
        {
            return _id;
        }
        set
        {
            lastId = value;
            _id = lastId++;
            _isIdOverridden = true;
        }
    }
    private int _id;
    bool _isIdOverridden = false;

    private void Start()
    {
        if (_isIdOverridden == false)
        {
            _id = lastId++;
        }

    }

    public string GetName()
    {
        return _overviewName;
    }
}
