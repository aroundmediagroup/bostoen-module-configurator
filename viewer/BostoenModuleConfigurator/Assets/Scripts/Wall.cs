using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WallType
{
    Front = 0,
    Back  = 1,
    Left  = 2,
    Right = 3
}

public class Wall : MonoBehaviour
{
    [SerializeField] private WallPiece _wallPiecePrefab;
    [SerializeField] private WallType _wallType;

    public WallType WallType
    {
        get
        {
            return _wallType;
        }
    }

    public Room ParentRoom = null;

    private List<WallPiece> _wallsPiecePool = new List<WallPiece>();

    private List<WallPiece> _currenActiveWallPieces = new List<WallPiece>();

    public void SetPosition(Vector3 newPosition)
    {
        transform.localPosition = newPosition;
    }

    public void SetLength(float newLength)
    {
        SetAllPiecesActive(false);
        _currenActiveWallPieces.Clear();
        int panelCount = (int)newLength;
        for (int i = 0; i < panelCount; i++)
        {
            WallPiece wallPiece = GetWallPiece();
            wallPiece.ParentWall = this;
            wallPiece.transform.localPosition = new Vector3(0, 0, -i);
            _currenActiveWallPieces.Add(wallPiece);
        }
    }

    public int GetWallPieceIndex(WallPiece wallPiece)
    {
        if (_currenActiveWallPieces.Contains(wallPiece))
        {
            return _currenActiveWallPieces.IndexOf(wallPiece);
        }
        return -1;
    }

    public WallPiece GetWallPieceAtIndex(int index)
    {
        return _currenActiveWallPieces[index];
    }

    public int GetWallPieceCount()
    {
        return _currenActiveWallPieces.Count;
    }

    private void SetAllPiecesActive(bool isActive)
    {
        for (int i = 0; i < _currenActiveWallPieces.Count; i++)
        {
            _currenActiveWallPieces[i].SetActive(isActive);
        }
    }

    private WallPiece GetWallPiece()
    {
        WallPiece wall = _wallsPiecePool.Find(piece => piece.gameObject.activeSelf == false);
        if (wall == null)
        {
            wall = Instantiate(_wallPiecePrefab, transform);
            _wallsPiecePool.Add(wall);
        }
        else
        {
            wall.SetVisble(true);
            wall.SetActive(true);
        }
        return wall;
    }
}
