const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./src/external/routes/index');
const apiCreateSessionRouter = require('./src/external/api-calls/create-session');
const apiUpdateSessionRouter = require('./src/external/api-calls/update-session');
const apiGetSessionRouter = require('./src/external/api-calls/get-session');
const dotenv = require('dotenv').config();
//const BaseConnector = require('./src/adapters/connectors/baseConnector.js');
//const AirtableConnector = require('./src/adapters/connectors/airtableConnector.js');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
const appPath = path.join(__dirname, 'static');
app.use('/static/', express.static(appPath));

app.use('/', indexRouter);
app.use(apiCreateSessionRouter);
app.use(apiUpdateSessionRouter);
app.use(apiGetSessionRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//BaseConnector.initialize(new AirtableConnector());
//BaseConnector.initialize(new BaseConnector());

module.exports = app;
