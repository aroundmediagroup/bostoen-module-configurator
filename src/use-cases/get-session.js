module.exports = function makeGetSession ({ sessionsDb }) {
    return async function getSession (id) {
        return sessionsDb.findById({ id: id });
    }
}