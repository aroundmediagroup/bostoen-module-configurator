module.exports = function makeUpdateSession ({ sessionsDb }) {
  return async function updateSession (id, session) {
    return sessionsDb.update({ id: id, session: session });
  }
}