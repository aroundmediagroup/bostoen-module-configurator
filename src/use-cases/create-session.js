module.exports = function makeCreateSession ({ sessionsDb }) {
  return async function createSession (session) {
    return sessionsDb.insert({ session: session });
  }
}