const GLOBALS = require('./globals.js');

function resetTooltip(toolTipElementId, text)
{
    let tooltip = document.getElementById(toolTipElementId);
    tooltip.innerText = text;
}

function innerTextToClipboard(element, toolTipElementId, tooltipText)
{
    if (!element) return;
    if (!navigator.clipboard) return;
    //
    let text = element.innerText;
    GLOBALS.SHOWLOGS && console.log(`copying code '${text}' to clipboard`);
    try
    {
        navigator.clipboard.writeText(text);
        //
        let tooltip = document.getElementById(toolTipElementId);
        tooltip.innerText = tooltipText;
        //
    } catch (err)
    {
        console.error('Failed to copy!', err);
    }
}

function hideElement(elementId)
{
    let element = document.getElementById(elementId);
    element.style.display = "none";
}

function showElement(elementId)
{
    let element = document.getElementById(elementId);
    element.style.display = "block"
}

function parseQueryUrl(callback) {
    let urlParams;
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query)) {
       urlParams[decode(match[1])] = decode(match[2]);
    }
    GLOBALS.SHOWLOGS && console.log('returning urlparameters');
    GLOBALS.SHOWLOGS && console.log(urlParams);
    callback(urlParams);
};

function isTouchDevice() {
    return 'ontouchstart' in document.documentElement;
}

module.exports = { resetTooltip, innerTextToClipboard, hideElement, showElement, parseQueryUrl, isTouchDevice }