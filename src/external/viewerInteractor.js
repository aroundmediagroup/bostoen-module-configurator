const GLOBALS = require('../globals.js');

class ViewerInteractor
{
    constructor(viewerWindow, serverUrl)
    {
        this.onViewerLoaded = null;
        this.viewerWindow = viewerWindow;
        this.serverUrl = serverUrl;
    }

    onMessageReceived(message)
    {
        console.log(message);
        const messageData = JSON.parse(message.data);
        GLOBALS.SHOWLOGS && console.log(`ViewerInteractor: Received message from parent ${messageData.type}`);
        if (messageData.type == 'ViewerLoadedEvent')
        {
            this.onViewerLoaded(messageData);
        }
    };

    initializeViewerCanvas()
    {
        console.log("ViewerInteractor: Initializing ViewerCanvas");
        this.sendEvent(new InitializeViewerCanvasEvent());
    }

    sendInitializeViewerEvent(sessionId = "")
    {
        let initializeViewerEvent = new SendInitializeViewerEvent(sessionId);
        initializeViewerEvent.showLogs = GLOBALS.SHOWLOGS;
        this.sendEvent(initializeViewerEvent);
    }

    sendEvent(event)
    {
        if(this.viewerWindow == null) return console.error('ViewerInteractor: trying to sendEvent while viewerWindow is null');
        this.viewerWindow.postMessage(event, '*');
    }

    onViewerLoaded()
    {
        GLOBALS.SHOWLOGS && console.log("ViewerInteractor: Viewer loaded");
        if (this.onViewerLoaded) this.onViewerLoaded();
    }
}

class PostedEvent
{
    constructor(type)
    {
        this.type = type;
    }
}

class ViewerLoadedEvent extends PostedEvent
{
    constructor()
    {
        super("ViewerLoadedEvent");
    }
}

class InitializeViewerCanvasEvent extends PostedEvent
{
    constructor()
    {
        super("initializeViewerCanvas");
        console.log("Send: initializeViewerCanvas");
    }
}

class SendInitializeViewerEvent extends PostedEvent
{
  constructor(sessionId, serverUrl)
  {
    super("InitializeViewerEvent");
    this.sessionId = sessionId;
    this.serverUrl = serverUrl;
  }
}

module.exports = ViewerInteractor;