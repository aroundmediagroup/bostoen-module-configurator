const { Storage } = require('@google-cloud/storage');
const stream = require('stream');

module.exports = function makeGoogleBucket ({ bucket: bucketName }) {
  return Object.freeze({
    getObject,
    storeObject,
    deleteObject,
    updateObject
  });

  function getBucket() {
    var storage = new Storage();
    return storage.bucket(bucketName);
  }

  async function storeObject(path, data)
    {
      return new Promise(async (resolve, reject) => {
        var bufferStream = new stream.PassThrough();
        bufferStream.end(Buffer.from(JSON.stringify(data)));

        var file = getBucket().file(path);
        bufferStream.pipe(file.createWriteStream({ public: true }))
        .on('error', (error) => { return reject(error); })
        .on('finish', () => { return resolve(); });
      });
    }

    async function getObject(path)
    {
      return new Promise(async (resolve, reject) => {
        try {
          const file = getBucket().file(path);
          const fileExists = await file.exists();
          if(!fileExists) return resolve({ isFound: false, data: null, error: null });

          var bufferStream = file.createReadStream();

          var  data = '';
          bufferStream
          .on('data', (chunk) => { data += chunk; })
          .on('error', (error) => { return reject(error); })
          .on('end', () => { return resolve({ isFound: true, data: JSON.parse(data), error: null }); });
        }
        catch (err) {
          return resolve({ isFound: false, data: null, error: err });
        }
      });
    }

    async function deleteObject(path) {
      return new Promise(async (resolve, reject) => {
        getBucket().file(path).delete()
        .then(() => { resolve(); })
        .catch(error => { reject(error); })
      });
    }

    async function updateObject(path, data) {
      return new Promise(async (resolve, reject) => {
        deleteObject(path)
        .then(() => {
          storeObject(path, data)
          .then((result) => { return resolve(result); })
          .catch((error) => { reject(error); })
        })
        .catch((error) => { return reject(error); })
      });
    }
}