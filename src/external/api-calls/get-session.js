const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler')
const makeSessionsDb = require('../../adapters/data-access/sessions-db');
const makeGetSession = require('../../use-cases/get-session');
const makeGoogleBucket = require('../data-access/google-bucket');
const {v4 : uuidv4} = require('uuid');

router.get('/api/get-session/:sessionId', async (request, response, next) =>
{
  const sessionId = request.params.sessionId;

  console.log("Session to retrieve:");
  console.log(sessionId);


  const sessionsDb = makeSessionsDb({ makeDb: makeGoogleBucket({ bucket: 'bostoen-modular-configurator'} ), generateId: uuidv4 });
  const getSession = makeGetSession({ sessionsDb: sessionsDb });

  getSession(sessionId)
  .then((result) => { response.json({ status: 'success', isFound: result.isFound, session: result.data, error: result.error }); })
  .catch((error) => { response.json({ status: 'failure', error: error }); });
});

module.exports = router;