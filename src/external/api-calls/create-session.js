const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler')
const makeSessionsDb = require('../../adapters/data-access/sessions-db');
const makeCreateSession = require('../../use-cases/create-session');
const makeGoogleBucket = require('../data-access/google-bucket');
const {v4 : uuidv4} = require('uuid');

router.post('/api/create-session', async (request, response, next) =>
{
  const sessionData = request.body.session;

  console.log("Session to create:");
  console.log(JSON.stringify(sessionData));


  const sessionsDb = makeSessionsDb({ makeDb: makeGoogleBucket({ bucket: 'bostoen-modular-configurator'} ), generateId: uuidv4 });
  const createSession = makeCreateSession({ sessionsDb: sessionsDb });

  createSession(sessionData)
  .then((result) => { response.json({ status: 'success', id: result.id }); })
  .catch((error) => { response.json({ status: 'failure', error: error }); });
});

module.exports = router;