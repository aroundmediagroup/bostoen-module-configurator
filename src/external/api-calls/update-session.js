const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler')
const makeSessionsDb = require('../../adapters/data-access/sessions-db');
const makeUpdateSession = require('../../use-cases/update-session');
const makeGoogleBucket = require('../data-access/google-bucket');
const {v4 : uuidv4} = require('uuid');

router.post('/api/update-session', async (request, response, next) =>
{
  const sessionData = request.body.session;
  const sessionId = request.body.id;
  console.log("Session to update:");
  console.log(sessionId);
  console.log(JSON.stringify(sessionData));

  const sessionsDb = makeSessionsDb({ makeDb: makeGoogleBucket({ bucket: 'bostoen-modular-configurator'} ), generateId: uuidv4 });
  const updateSession = makeUpdateSession({ sessionsDb: sessionsDb });

  updateSession(sessionId, sessionData)
  .then((result) => { response.json({ status: 'success' }); })
  .catch((error) => { response.json({ status: 'failure', error: error }); });
});

module.exports = router;