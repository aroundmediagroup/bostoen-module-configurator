class Session
{
    constructor(slotOptions = {}, timeStamp = Date.now())
    {
        this.slotOptions = slotOptions;
        this.timeStamp = timeStamp;
    }
}

module.exports = Session;