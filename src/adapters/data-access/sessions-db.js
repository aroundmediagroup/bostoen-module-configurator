
module.exports = function makeSessionsDb ({ makeDb, generateId }) {
  return Object.freeze({
    findById,
    insert,
    remove,
    update
  })

  async function findById ({ id: _id }) {
    return new Promise(async (resolve, reject) => {
      makeDb.getObject(getFilePath(_id))
      .then((result) => { resolve(result); })
      .catch((error) => { reject({ error: error }); });
    });
  }

  async function insert ({ id: _id = generateId(), session: sessionInfo }) {
    return new Promise(async (resolve, reject) => {
      makeDb.storeObject(getFilePath(_id), sessionInfo)
      .then((result) => { resolve({ id: _id }); })
      .catch((error) => { reject({ id: null, error: error }); });
    });
  }

  async function update ({ id: _id, session: sessionInfo }) {
    return new Promise(async (resolve, reject) => {
      makeDb.updateObject(getFilePath(_id), sessionInfo)
      .then((result) => { resolve(result); })
      .catch((error) => { reject({ error: error }); });
    });
  }

  async function remove ({ id: _id }) {
    return new Promise(async (resolve, reject) => {
      makeDb.deleteObject(getFilePath(_id))
      .then((result) => { resolve(result); })
      .catch((error) => { reject({ error: error }); });
    });
  }

  function getFilePath(name) {
    return `sessions/${name}.json`;
  }
}