const BaseConnector = require('./baseConnector.js');
const AirtablePlus = require('airtable-plus');
const Session = require('../../entities/sessions/session.js');
const asyncHandler = require('express-async-handler')
const ModelSlot = require('../../entities/modelslot.js');
const ModelOption = require('../../entities/modeloption.js');
const fs = require('fs');

class AirtableConnector extends BaseConnector
{
    constructor()
    {
        super();
    }

    openConnectionToTable(tableName)
    {
        // Setup connection with API Key
        const baseId = 'appunMc2aMOMk6INj';
        const connection = new AirtablePlus({
            apiKey: process.env.AIRTABLE_API_KEY,
            baseID: baseId,
            tableName: tableName
        });

        return connection;
    }

    async getSceneSetup(sceneName)
    {
        return new Promise(async (resolve, reject) =>
        {
            let scene;
            if (sceneName == undefined)
            {
                reject("sceneName is empty!");
                return;
            }

            const foundRow = await this.openConnectionToTable('Scenes').read({
                maxRecords: 1,
                filterByFormula: `{SceneName} = '${sceneName}'`
            }).catch(err =>
            {
                console.error(err);
                return;
            });

            if (foundRow.length == 0) reject('Scene not found');
            scene = foundRow[0];

            let configurationObject = {};

            try
            {
                const sceneViewIds = scene.fields['SceneViewIds'];
                const sceneViewNames = scene.fields['SceneViewNames'];
                const rootNodeName = scene.fields['RootNodeName'];
                const sceneId = scene.fields['SceneName'];

                let views = {};
                for (let i = 0; i < sceneViewNames.length; i++)
                {
                    const sceneViewName = sceneViewNames[i];
                    views[sceneViewIds[i]] = sceneViewName;
                }

                const initialView = scene.fields['View Name (from InitialView)'][0];
                const modelSlots = await this._getModelSlotsFromAirtable(scene.fields['Slots']).catch(error => reject(error));
                const sceneUrl = scene.fields['Scene URL'];

                configurationObject = {
                    initialView: initialView,
                    views: views,
                    modelSlots: modelSlots,
                    sceneUrl: sceneUrl,
                    rootNodeName: rootNodeName,
                    sceneId: sceneId
                }
            } catch (error)
            {
                reject(error);
            }


            //console.log(configurationObject);
            resolve(configurationObject);
        });
    }

    async getConfigurationObject(sceneName)
    {
        return new Promise(async (resolve, reject) =>
        {
            console.log(`getConfigurationObject('${sceneName}')`);
            const path = 'datastorage/scenes/' + sceneName + '.json';
            try {
                resolve(await JSON.parse(fs.readFileSync(path, 'utf8')));
              } catch (err) {
                reject(err);
                return false
              }
        });
    }

    async saveSession(sessionId, sessionData)
    {
        return new Promise(async (resolve, reject) => {
            const timeStamp = sessionData.timeStamp;
            try {
                this.openConnectionToTable('Sessions')
                .create({
                    "SessionCode": sessionId,
                    "TimeStamp": timeStamp,
                    "configuration_data": JSON.stringify(sessionData)
                })
                .then((res) => {
                    resolve(res);
                })
                .catch(err =>
                {
                    console.warn(err);
                    reject(err);
                });
            } catch (err)
            {
                console.warn(err);
                reject(err);
            }

        });
    }

    async getSession(sessionId)
    {
        return new Promise(async (resolve, reject) =>
        {
            if (sessionId == undefined)
            {
                reject("sessionId is empty!");
                return null;
            }

            const foundRow = await this.openConnectionToTable('Sessions').read({
                maxRecords: 1,
                filterByFormula: `{SessionCode} = '${sessionId}'`
            }).catch(err =>
            {
                console.error(err);
                reject("Error while trying to retrieve a session");
                return;
            });

            if (foundRow.length == 0)
            {
                reject("Session with sessionId '" + sessionId + "' not found");
                return null;
            }

            let row = foundRow[0];
            console.log('Session has been found');

            let configData = row.fields["configuration_data"];
            //let sessionData = JSON.parse(configData);
            /*
            let timeStamp = Date(row.fields['TimeStamp']);


            let slotOptions = {};
            for (let j = 0; j < sessionData.length; j++)
            {
                slotOptions[sessionData[j]] = sessionData[j];
            }

            let foundSession = JSON.parse(configData) //new Session(slotOptions, timeStamp);
            */
            //console.log(foundSession);

            resolve(configData);
        });
    }

    async _getSlotOptionIds(slotOptionsObjected)
    {
        return new Promise(async (resolve, reject) =>
        {
            let result = {};
            //console.log('_getSlotOptionIds');
            //console.log(slotOptionsObjected);

            const slotNames = Object.keys(slotOptionsObjected);
            const optionNames = Object.values(slotOptionsObjected);

            const slotIds = await this._getSlotIds(slotNames).catch(err =>
            {
                reject(err);
                return;
            });
            const optionIds = await this._getOptionIds(optionNames).catch(err =>
            {
                reject(err);
                return;
            });

            result.slotIds = slotIds;
            result.optionIds = optionIds;
            resolve(result);
        });
    }

    async _getSlotIds(slotNames)
    {
        return new Promise(async (resolve, reject) =>
        {
            //console.log('_getSlotIds');
            //console.log(slotNames);
            let slotIds = [];

            try
            {
                const foundRows = await this.openConnectionToTable('Slots').read({
                    maxRecords: slotNames.length,
                    filterByFormula: this._getFilterByNameArray(slotNames, 'SlotName')
                }).catch(err =>
                {
                    reject(err);
                    return;
                });
                if (foundRows.length == 0)
                {
                    reject('_getSlotIds: foundRows.length == 0');
                    return;
                }

                let resultSlotNames = [];
                for (let i = 0; i < foundRows.length; i++)
                {
                    const row = foundRows[i];
                    resultSlotNames.push(row.fields['SlotName']);
                }

                if (foundRows.length != slotNames.length)
                {
                    reject('Not all slots were found.');
                    return;
                }

                for (let i = 0; i < slotNames.length; i++)
                {
                    const slotName = slotNames[i];
                    let index = resultSlotNames.indexOf(slotName);
                    let linkedRow = foundRows[index];
                    if (linkedRow != undefined)
                    {
                        slotIds.push(linkedRow.id);
                    }
                    else
                    {
                        reject("linkedRow == undefined: " + slotName);
                        return;
                    }
                }
            }
            catch (err)
            {
                reject(err);
                return;
            }

            resolve(slotIds);
            return;
        });
    }

    async _getOptionIds(optionNames)
    {
        //console.log('_getOptionIds');
        //console.log(optionNames);
        return new Promise(async (resolve, reject) =>
        {
            let optionIds = [];
            try
            {
                const foundRows = await this.openConnectionToTable('Options').read({
                    maxRecords: optionNames.length,
                    filterByFormula: this._getFilterByNameArray(optionNames, 'OptionName')
                }).catch(err =>
                {
                    reject(err);
                    return;
                });
                if (foundRows.length == 0)
                {
                    reject('no options found');
                    return;
                }

                let resultOptionNames = [];
                for (let i = 0; i < foundRows.length; i++)
                {
                    const row = foundRows[i];
                    resultOptionNames.push(row.fields['OptionName']);
                }

                for (let i = 0; i < optionNames.length; i++)
                {
                    const optionName = optionNames[i];
                    let index = resultOptionNames.indexOf(optionName);
                    let linkedRow = foundRows[index];
                    if (linkedRow != undefined)
                    {
                        optionIds.push(linkedRow.id);
                    }
                    else
                    {
                        reject("linkedRow == undefined: " + optionName);
                        return;
                    }
                }

                let numOfNone = 0;
                for(let i=0; i < optionNames.length; i++) {
                    if(optionNames[i] == "none") numOfNone = numOfNone + 1;
                }
                if(numOfNone != 0) numOfNone = numOfNone - 1;

                if (resultOptionNames.length != optionNames.length - numOfNone)
                {
                    let response = {
                        message: 'Not all options were found.',
                        optionNames: optionNames,
                        resultOptionNames: resultOptionNames,
                        numOfNone: numOfNone
                    };
                    reject(response);
                    return;
                }

            }
            catch (err)
            {
                reject(err);
                return;
            }

            resolve(optionIds);
            return;
        });
    }

    _getFilterByNameArray(namesToFilter, fieldName)
    {
        if (namesToFilter.length == 0) return "";

        //{OptionName} = '${optionNames}
        //`OR({${fieldName}} = '${optionNames}', {OptionName} = '${optionNames}')`
        let filter = '';
        for (let i = 0; i < namesToFilter.length; i++)
        {
            let name = namesToFilter[i];
            filter += `{${fieldName}} = '${name}'`;
            if (i < namesToFilter.length - 1) filter += ',';
        }

        if (namesToFilter.length > 1)
        {
            filter = `OR(${filter})`;
        }

        return filter;
    }

    _getModelSlotsFromAirtable(linkedSlotIds)
    {
        return new Promise(async (resolve, reject) =>
        {
            //console.log(linkedSlotIds);
            let modelSlots = [];

            for (let i = 0; i < linkedSlotIds.length; i++)
            {
                const linkedSlotId = linkedSlotIds[i];
                const foundSlot = await this.openConnectionToTable('Slots').find(linkedSlotId).catch(err =>
                {
                    reject(err);
                    return;
                });

                let modelSlot = new ModelSlot(foundSlot.fields['SlotName']);
                modelSlot.view = foundSlot.fields['View Name (from View)'][0];
                modelSlot.defaultOptionIndex = foundSlot.fields['DefaultOptionIndex'];

                const optionIds = foundSlot.fields['Options'];
                let modelOptions = [];
                for (let j = 0; j < optionIds.length; j++)
                {
                    const optionId = optionIds[j];
                    const foundOption = await this.openConnectionToTable('Options').find(optionId).catch(err =>
                    {
                        reject(err);
                        return;
                    });

                    let modelOption = new ModelOption(foundOption.fields['Title'], foundOption.fields['OptionName']);
                    const iconFields = foundOption.fields['Icon'];
                    if(iconFields != undefined && iconFields.length > 0)
                    {
                        modelOption.imageUrl = foundOption.fields['Icon'][0].url;
                    }
                    modelOptions.push(modelOption);
                }
                modelSlot.modelOptions = modelOptions;

                modelSlots.push(modelSlot);
            }

            resolve(modelSlots);
        });
    }
}

module.exports = AirtableConnector;