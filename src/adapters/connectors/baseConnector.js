class BaseConnector
{
    static initialize(connectorType)
    {
        BaseConnector._instance = connectorType;
    }

    static getInstance()
    {
        return BaseConnector._instance;
    }

    constructor()
    {
        this.testingStorage = new Map();
    }

    async getConfigurationObject(sceneName)
    {
        let configurationObject = null;
        if(sceneName == "jumatt_configurator")
        {
            configurationObject = {
                initialView: this._getInitialViewJumat(),
                views: this._getViewsJumat(),
                modelSlots: this._getModelSlotsJumat(),
                sceneUrl: 'https://viewit.prompto.com/jumatt_configurator/',
                rootNodeName: 'jumatt_configurator'
            }
        }
        else
        {
        configurationObject = {
            initialView: this._getInitialView(),
            views: this._getViews(),
            modelSlots: this._getModelSlots(),
            sceneUrl: this._getSceneUrl(),
            rootNodeName: 'Scene'
        }
        }
        return configurationObject;
    }

    async saveSession(sessionId, session)
    {
        this.testingStorage.set(sessionId, session);
        console.log(this.testingStorage);
        
        let success = true;
        return success;
    }


    getSession(sessionId)
    {
        console.log('Getting session for code: ' + sessionId);

        const Session = require('../sessions/session.js');

        //console.log(this.testingStorage.)
        let foundSession = this.testingStorage.get(sessionId);
        if (foundSession == undefined) return null;

        console.log(foundSession);
        let session = new Session(foundSession.slotOptions, foundSession.timeStamp);

        console.log(session);

        return session;
        /*
          let chosenOptions = {};
          chosenOptions['Floor.1.Slot'] = 'Floor.1.Module.B';
          chosenOptions['Floor.0.SlotA'] = 'Floor.0.Module.A';
          chosenOptions['Floor.0.SlotB'] = 'Floor.0.Module.LB';
        
          let dataTime = new Date(2020, 11, 11, 11, 11);
          
          let session = new Session(chosenOptions, dataTime);
          
          console.log(session);
        
          return session;
        */
    }

    _getModelSlots()
    {
        var ModelSlot = require('../modelslot.js');
        var ModelOption = require('../modeloption.js');

        // add options
        let modelSlots = [];
        let modelSlot = null;
        let modelOption = null;

        modelSlot = new ModelSlot("Floor.1.Slot");
        modelSlot.view = "Floor.1";
        modelOption = new ModelOption("Module A", "Floor.1.Module.A");
        modelOption.imageUrl = '../../images/options/Floor1_OptionA.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelOption = new ModelOption("Module B", "Floor.1.Module.B");
        modelOption.imageUrl = '../../images/options/Floor1_OptionB.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelSlots.push(modelSlot);

        modelSlot = new ModelSlot("Floor.0.SlotA");
        modelSlot.view = "Floor.0";
        modelOption = new ModelOption("Module A", "Floor.0.Module.A");
        modelOption.imageUrl = '../../images/options/Floor0_OptionA.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelOption = new ModelOption("Module B", "Floor.0.Module.B");
        modelOption.imageUrl = '../../images/options/Floor0_OptionB.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelSlots.push(modelSlot);

        modelSlot = new ModelSlot("Floor.0.SlotB");
        modelSlot.view = "Floor.0";
        modelOption = new ModelOption("Living A", "Floor.0.Module.LA");
        modelOption.imageUrl = '../../images/options/Floor0_OptionLA.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelOption = new ModelOption("Living B", "Floor.0.Module.LB");
        modelOption.imageUrl = '../../images/options/Floor0_OptionLB.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelSlots.push(modelSlot);

        return modelSlots;
    }

    _getInitialView()
    {
        let initialView = "Floor.0";
        return initialView;
    }

    _getViews()
    {
        let views = {};
        views['Floor.0'] = 1;
        views['Floor.1'] = 4;

        return views;
    }

    _getSceneUrl()
    {
        return 'https://viewit.prompto.com/bostoen-module-configurator/';
    }


    _getModelSlotsJumat()
    {
        var ModelSlot = require('../modelslot.js');
        var ModelOption = require('../modeloption.js');

        // add options
        let modelSlots = [];
        let modelSlot = null;
        let modelOption = null;

        modelSlot = new ModelSlot("+000_Opening1");
        modelSlot.view = "Floor.0";
        modelOption = new ModelOption("Window Floor", "+000_Opening1_WindowFloor_$AssimpFbx$_GeometricTranslation");
        modelOption.imageUrl = '../../images/options/Floor1_OptionA.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelOption = new ModelOption("Window", "+000_Opening1_Window_$AssimpFbx$_GeometricTranslation");
        modelOption.imageUrl = '../../images/options/Floor1_OptionB.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelOption = new ModelOption("Window", "+000_Opening1_Closed");
        modelOption.imageUrl = '../../images/options/Floor1_OptionB.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelSlots.push(modelSlot);

        modelSlot = new ModelSlot("+000_Bijgebouw2");
        modelSlot.view = "Floor.0";
        modelOption = new ModelOption("Window Floor", "+000_Bijbouw2_WindowFloor_$AssimpFbx$_Rotation");
        modelOption.imageUrl = '../../images/options/Floor1_OptionA.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelOption = new ModelOption("Window", "+000_Bijbouw2_Window_$AssimpFbx$_Translation");
        modelOption.imageUrl = '../../images/options/Floor1_OptionB.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelOption = new ModelOption("Window", "+000_bijbouw2_closed_$AssimpFbx$_Translation");
        modelOption.imageUrl = '../../images/options/Floor1_OptionB.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelSlots.push(modelSlot);

        modelSlot = new ModelSlot("+000_Garage");
        modelSlot.view = "Floor.0";
        modelOption = new ModelOption("Garage", "+000_Garage_yes");
        modelOption.imageUrl = '../../images/options/Floor1_OptionA.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelOption = new ModelOption("No Garage", "+000_Garage_empty");
        modelOption.imageUrl = '../../images/options/Floor1_OptionB.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelSlots.push(modelSlot);



        modelSlot = new ModelSlot("BlokA_Indelingen");
        modelSlot.view = "Floor.1";
        modelOption = new ModelOption("Indeling 1", "+001_A_Indeling1_$AssimpFbx$_Translation");
        modelOption.imageUrl = '../../images/options/Floor1_OptionA.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelOption = new ModelOption("Indeling 2", "+001_BlokA_Indeling2");
        modelOption.imageUrl = '../../images/options/Floor1_OptionB.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelOption = new ModelOption("Indeling 3", "+001_BlokA_Indeling3");
        modelOption.imageUrl = '../../images/options/Floor1_OptionB.jpg'
        modelSlot.modelOptions.push(modelOption);
        modelSlots.push(modelSlot);
        
        return modelSlots;
    }

    _getInitialViewJumat()
    {
        let initialView = "Floor.0";
        return initialView;
    }

    _getViewsJumat()
    {
        let views = {};
        views['Floor.0'] = 1;
        views['Floor.1'] = 4;

        return views;
    }
}

module.exports = BaseConnector;