const GLOBALS = require('../globals.js');

class ConfigurationSetupManager
{
    constructor()
    {
        this.sceneConfigurationData = null;
    }

    setSceneConfigurationData(sceneSetup)
    {
        this.sceneConfigurationData = sceneSetup;
    }

    getModelSlots()
    {
        if(this.sceneConfigurationData == null)
        {
            console.error('Trying to access data that hasnt been retrieved yet! Call loadSceneConfigurationData first!');
            return null;
        }
        GLOBALS.SHOWLOGS && console.log('the modelslots retrieved from the backend are:');
        GLOBALS.SHOWLOGS && console.log(this.sceneConfigurationData.modelSlots);
        return this.sceneConfigurationData.modelSlots;
    }

    getInitialView()
    {
        if(this.sceneConfigurationData == null)
        {
            console.error('Trying to access data that hasnt been retrieved yet! Call loadSceneConfigurationData first!');
            return null;
        }
        return this.sceneConfigurationData.initialView;
    }

    getViews()
    {
        if(this.sceneConfigurationData == null)
        {
            console.error('Trying to access data that hasnt been retrieved yet! Call loadSceneConfigurationData first!');
            return null;
        }
        return this.sceneConfigurationData.views;
    }

    getSceneUrl()
    {
        if(this.sceneConfigurationData == null)
        {
            console.error('Trying to access data that hasnt been retrieved yet! Call loadSceneConfigurationData first!');
            return null;
        }
        return this.sceneConfigurationData.sceneUrl;
    }

    getRootNodeName()
    {
        return this.sceneConfigurationData.rootNodeName;
    }
}

module.exports = ConfigurationSetupManager;