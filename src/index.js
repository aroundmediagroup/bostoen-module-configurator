const HelperFunctions = require('./helperFunctions');
const ViewerInteractor = require('./external/viewerInteractor');
const GLOBALS = require('./globals');

var viewerCanvas = document.getElementById('viewer-canvas');

const viewerWindow = viewerCanvas.contentWindow;
const viewerInteractor = new ViewerInteractor(viewerWindow);

var loadScreen = document.getElementById('loader');

HelperFunctions.hideElement("configurator-interface");

GLOBALS.SHOWLOGS && console.log('initialize config web page');
window.onpopstate = HelperFunctions.parseQueryUrl(onMainWidowOpened);

var sessionId = "";

var mainWindowLoaded = false;
async function onMainWidowOpened(urlParams)
{
    if (urlParams['debug'] != undefined && urlParams['debug'].toLowerCase() == 'true') GLOBALS.DEBUG = true;
    if (urlParams['showlogs'] != undefined && urlParams['showlogs'].toLowerCase() == 'true') GLOBALS.SHOWLOGS = true;
    if (urlParams['noiframe'] != undefined && urlParams['noiframe'].toLowerCase() == 'true') GLOBALS.NOIFRAME = true;

    console.log('DEBUG:' + GLOBALS.DEBUG);
    console.log('SHOWLOGS:' + GLOBALS.SHOWLOGS);

    if(urlParams['session'] != undefined) sessionId = urlParams['session'];

    /*
    if(HelperFunctions.isTouchDevice())
    {

    }


    GLOBALS.SHOWLOGS && console.log('window opened');
    const isTestingUI = GLOBALS.NOIFRAME;
    if(isTestingUI) ShowUI();*/
    mainWindowLoaded = true;
    tryInitializeViewer();
}

var viewerCanvasLoaded = false;
viewerCanvas.onload = function()
{
    viewerCanvasLoaded = true;
    viewerInteractor.initializeViewerCanvas();
    tryInitializeViewer();
};

function tryInitializeViewer()
{
    if(mainWindowLoaded && viewerCanvasLoaded)
    {
        if(viewerCanvas.src != "")
        {
            viewerInteractor.sendInitializeViewerEvent(sessionId);
        }
    }
}

window.onmessage = function(message) { viewerInteractor.onMessageReceived(message) };

// 3D scene loaded
viewerInteractor.onViewerLoaded = () => {
    console.log('viewer loaded');
    hideLoadScreen();
}

function hideLoadScreen()
{
    loadScreen.classList.add('fadeout');
    setTimeout(function () {
        loadScreen.style.display = 'none';
      }, 2000);
}