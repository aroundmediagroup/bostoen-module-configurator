(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
const GLOBALS = require('../globals.js');

class ViewerInteractor
{
    constructor(viewerWindow, serverUrl)
    {
        this.onViewerLoaded = null;
        this.viewerWindow = viewerWindow;
        this.serverUrl = serverUrl;
    }

    onMessageReceived(message)
    {
        console.log(message);
        const messageData = JSON.parse(message.data);
        GLOBALS.SHOWLOGS && console.log(`ViewerInteractor: Received message from parent ${messageData.type}`);
        if (messageData.type == 'ViewerLoadedEvent')
        {
            this.onViewerLoaded(messageData);
        }
    };

    initializeViewerCanvas()
    {
        console.log("ViewerInteractor: Initializing ViewerCanvas");
        this.sendEvent(new InitializeViewerCanvasEvent());
    }

    sendInitializeViewerEvent(sessionId = "")
    {
        let initializeViewerEvent = new SendInitializeViewerEvent(sessionId);
        initializeViewerEvent.showLogs = GLOBALS.SHOWLOGS;
        this.sendEvent(initializeViewerEvent);
    }

    sendEvent(event)
    {
        if(this.viewerWindow == null) return console.error('ViewerInteractor: trying to sendEvent while viewerWindow is null');
        this.viewerWindow.postMessage(event, '*');
    }

    onViewerLoaded()
    {
        GLOBALS.SHOWLOGS && console.log("ViewerInteractor: Viewer loaded");
        if (this.onViewerLoaded) this.onViewerLoaded();
    }
}

class PostedEvent
{
    constructor(type)
    {
        this.type = type;
    }
}

class ViewerLoadedEvent extends PostedEvent
{
    constructor()
    {
        super("ViewerLoadedEvent");
    }
}

class InitializeViewerCanvasEvent extends PostedEvent
{
    constructor()
    {
        super("initializeViewerCanvas");
        console.log("Send: initializeViewerCanvas");
    }
}

class SendInitializeViewerEvent extends PostedEvent
{
  constructor(sessionId, serverUrl)
  {
    super("InitializeViewerEvent");
    this.sessionId = sessionId;
    this.serverUrl = serverUrl;
  }
}

module.exports = ViewerInteractor;
},{"../globals.js":2}],2:[function(require,module,exports){
var GLOBALS = { SHOWLOGS: false, DEBUG: false, NOIFRAME: false };

module.exports = GLOBALS;
},{}],3:[function(require,module,exports){
const GLOBALS = require('./globals.js');

function resetTooltip(toolTipElementId, text)
{
    let tooltip = document.getElementById(toolTipElementId);
    tooltip.innerText = text;
}

function innerTextToClipboard(element, toolTipElementId, tooltipText)
{
    if (!element) return;
    if (!navigator.clipboard) return;
    //
    let text = element.innerText;
    GLOBALS.SHOWLOGS && console.log(`copying code '${text}' to clipboard`);
    try
    {
        navigator.clipboard.writeText(text);
        //
        let tooltip = document.getElementById(toolTipElementId);
        tooltip.innerText = tooltipText;
        //
    } catch (err)
    {
        console.error('Failed to copy!', err);
    }
}

function hideElement(elementId)
{
    let element = document.getElementById(elementId);
    element.style.display = "none";
}

function showElement(elementId)
{
    let element = document.getElementById(elementId);
    element.style.display = "block"
}

function parseQueryUrl(callback) {
    let urlParams;
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query)) {
       urlParams[decode(match[1])] = decode(match[2]);
    }
    GLOBALS.SHOWLOGS && console.log('returning urlparameters');
    GLOBALS.SHOWLOGS && console.log(urlParams);
    callback(urlParams);
};

function isTouchDevice() {
    return 'ontouchstart' in document.documentElement;
}

module.exports = { resetTooltip, innerTextToClipboard, hideElement, showElement, parseQueryUrl, isTouchDevice }
},{"./globals.js":2}],4:[function(require,module,exports){
const HelperFunctions = require('./helperFunctions');
const ViewerInteractor = require('./external/viewerInteractor');
const GLOBALS = require('./globals');

var viewerCanvas = document.getElementById('viewer-canvas');

const viewerWindow = viewerCanvas.contentWindow;
const viewerInteractor = new ViewerInteractor(viewerWindow);

var loadScreen = document.getElementById('loader');

HelperFunctions.hideElement("configurator-interface");

GLOBALS.SHOWLOGS && console.log('initialize config web page');
window.onpopstate = HelperFunctions.parseQueryUrl(onMainWidowOpened);

var sessionId = "";

var mainWindowLoaded = false;
async function onMainWidowOpened(urlParams)
{
    if (urlParams['debug'] != undefined && urlParams['debug'].toLowerCase() == 'true') GLOBALS.DEBUG = true;
    if (urlParams['showlogs'] != undefined && urlParams['showlogs'].toLowerCase() == 'true') GLOBALS.SHOWLOGS = true;
    if (urlParams['noiframe'] != undefined && urlParams['noiframe'].toLowerCase() == 'true') GLOBALS.NOIFRAME = true;

    console.log('DEBUG:' + GLOBALS.DEBUG);
    console.log('SHOWLOGS:' + GLOBALS.SHOWLOGS);

    if(urlParams['session'] != undefined) sessionId = urlParams['session'];

    /*
    if(HelperFunctions.isTouchDevice())
    {

    }


    GLOBALS.SHOWLOGS && console.log('window opened');
    const isTestingUI = GLOBALS.NOIFRAME;
    if(isTestingUI) ShowUI();*/
    mainWindowLoaded = true;
    tryInitializeViewer();
}

var viewerCanvasLoaded = false;
viewerCanvas.onload = function()
{
    viewerCanvasLoaded = true;
    viewerInteractor.initializeViewerCanvas();
    tryInitializeViewer();
};

function tryInitializeViewer()
{
    if(mainWindowLoaded && viewerCanvasLoaded)
    {
        if(viewerCanvas.src != "")
        {
            viewerInteractor.sendInitializeViewerEvent(sessionId);
        }
    }
}

window.onmessage = function(message) { viewerInteractor.onMessageReceived(message) };

// 3D scene loaded
viewerInteractor.onViewerLoaded = () => {
    console.log('viewer loaded');
    hideLoadScreen();
}

function hideLoadScreen()
{
    loadScreen.classList.add('fadeout');
    setTimeout(function () {
        loadScreen.style.display = 'none';
      }, 2000);
}
},{"./external/viewerInteractor":1,"./globals":2,"./helperFunctions":3}]},{},[4]);
